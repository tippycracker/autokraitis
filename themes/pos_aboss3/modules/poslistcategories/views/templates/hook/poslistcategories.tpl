<div class="poslistcategories">
	<div class="container-fluid">
		<div class="pos_title">
			<h2>{l s='Our Categories' mod='poslistcategories'}</h2>
			<p>{l s='Typi non habent claritatem insitam est usus legentis in iis qui facit eorum claritatem.' mod='poslistcategories'}</p>
		</div>
		<div class="row  pos_content">
			<div class="block_content">
			{$count=0}
			{foreach from=$categories item=category name=poslistcategories}
				<div class="list-categories">
					{if $category.image}
					<div class="thumb-category">
						<a href="{$link->getCategoryLink($category['id_category'])}" target="_blank"><img src="{$link->getMediaLink("`$smarty.const._MODULE_DIR_`poslistcategories/images/`$category.image|escape:'htmlall':'UTF-8'`")}" alt="" /></a>
				
					</div>
					{/if}
					<div class="desc-listcategoreis">
						<div class="content-listcategoreis">
							<a href="{$link->getCategoryLink($category['id_category'])}" target="_blank">
							{if $category.categorythumb}
							<img src="{$category.categorythumb}" alt=""/>
							{else}
							<img src="{$link->getMediaLink("`$smarty.const._MODULE_DIR_`poslistcategories/images/")}menu_thumb.png"/>
							{/if} 
							</a>
							<div class="name_categories">
								<a href="{$link->getCategoryLink($category['id_category'])}" target="_blank">{$category.category_name} <span>({$category.nbProducts})</span></a>
								
							</div>
						</div>
						{if $category.description}
						<div class="description-list">
							<div class="desc-content">
								{$category.description}
							</div>
								
						</div>
						{/if}
					</div>		
				</div>			
				{$count= $count+1}
			{/foreach}		
			</div>
		</div>	
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		var poslistcategories = $(".poslistcategories .block_content");
		poslistcategories.owlCarousel({
			items : {$slider_options.number_item},
			itemsDesktop : [1199,{$slider_options.items_md}],
			itemsDesktopSmall : [991,{$slider_options.items_sm}],
			itemsTablet: [767,{$slider_options.items_xs}],
			itemsMobile : [479,{$slider_options.items_xxs}],
			autoPlay :  {if $slider_options.auto_play}{if $slider_options.delay}{$slider_options.delay}{else}3000{/if}{else} false{/if},
			slideSpeed : {if $slider_options.speed_slide}{$slider_options.speed_slide}{else}1000{/if},
			addClassActive: true,
			navigation : {if $slider_options.show_arrow} true {else} false {/if},
			pagination : {if $slider_options.show_pagination} true {else} false {/if},
		});
	});
</script>
