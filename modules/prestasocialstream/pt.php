<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_972578a14561371b21a83b76fa946633'] = 'Prestashop Social Stream';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_9bead94a9dee19dbe963be1723472711'] = 'Um fluxo de redes sociais';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_c888438d14855d7d96a2724ee9c306bd'] = 'Configurações atualizadas';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_254f642527b45bc260048e30704edb39'] = 'Configuração';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_a65974092ba2746503f6d6fb43c7976a'] = 'Comprimento do texto';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_87774cb3e7b7e2533b04c6539ce18b2e'] = 'O comprimento total do texto de cada post.';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_368d9ac76af05f714092bc808a426bfc'] = 'Cor de fundo';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_cafe1b94f70c18b134b6e2c57746fbce'] = 'Cor da guia (padrão ffffff)';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_168b82d33f8073018c50a4f658a02559'] = 'Colunas';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_6317436aa1fba4a2532fa57c3b24f359'] = 'Colunas para mostrar';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_52f5e0bc3859bc5f5e25130b6c7e8881'] = 'Posição';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_5c3a1b678d8a914c532b04a2dc8f1a16'] = 'Usuário do Facebook';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_442e70969317f4393d5635a4f5b7e8fe'] = 'Post para mostrar';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_21ba44deb46cc040dc907f4f317f6627'] = 'Total de post para mostrar';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_ebdadabe4adcc64e406d688efed24702'] = 'Token de acesso do Facebook';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_41241f56f39514b3b2e4a023a38bb12b'] = 'Usuário do Twitter';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_e5ee1149da83cf11c12a8b6a9080d083'] = 'Chave de consumidor do Twitter';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_e5d82a6a0710f10fcd96834b0703d80c'] = 'Criar um app aqui para obter os valores https://apps.twitter.com/';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_e5a4be39c3935ec602c2b7d390a4bffb'] = 'Segredo de consumidor do Twitter';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_5ba1d8bbbefc9f6c6b57796f05213a62'] = 'Usuário VK';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_5b2c8bfd1bc974966209b7be1cb51a72'] = 'Google +';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_76ca9a6b29f53abf17a1e3c7b5c69e56'] = 'Token de acesso do Google';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_579734c9a57322281cc0c195884e764c'] = 'Obter token de google: https://console.developers.google.com';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_3c41142b2301bd7d5306340f53bb7d50'] = 'ID de usuário do Instagram';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_8b366e3a0717cfe39ebc2a0f28be2e5a'] = 'ID de cliente do Instagram';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_cabda9ed458c48797ade702667888f12'] = 'Token de acesso do Instagram';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_6d5451eb221ef7c074bff66b432c829e'] = 'Veja o símbolo aqui: http://instagram.pixelunion.net/';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_647a4e084f66aedb60aa38724423ef48'] = 'Pinterest usuário';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_3219be871febbc2144357553cf523628'] = 'como \\\'@username/boardname\\\'. Use @ para o usuário e # para tags';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_a7f2ab01785f2c76885662d057965e6d'] = 'Token de acesso do Pinterest';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_32519363c740b400f7720d54b1ca5510'] = 'Programa Media';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_b4c30db236e2532035d4d5a35feb4cb9'] = 'Caixa de sombra';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_c611adc847f227280fb7f584d355cd5e'] = 'Tipo de sombra como: 0 1px 0 0 rgba (10, 10, 10, 0.2)';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_b89115a188150113cdd7504c1cf27f13'] = 'Palavras de baixo calão';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_a8f7501b932afaf4791616175dd3af77'] = 'Não dispplay as mensagens com estas palavras (como: word1, word2, word3,)';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_c9cc8cce247e49bae79f15173ce97354'] = 'Salvar';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_f4f70727dc34561dfde1a3c529b6205c'] = 'Configurações';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_3a6d2d67d8d3e7c3dbcc3b3982f12a0a'] = 'Velocidade da animação';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_0e058d928350704a0572507bd03c76b4'] = 'Velocidade da animação (200 padrão)';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_75794fe8ae8ecc3c2a6210e3ec3824cf'] = 'Margem da parte superior ou inferior';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_d0efb46cfd8b126482192a343e2142b4'] = 'Formato de imagem';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_3832fc9ce55fc92562c0e4b6aac54def'] = 'Technorati';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_e4c61e63437c8c315ba973a2fa1bb5f0'] = 'Technorati url';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_7d2c1dd0fe22673c11ccd481f46af07a'] = 'Url completo de sua página de Technorati (deixe em branco para usar o compartilhamento em vez de um link para o site)';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_898153e542ab70aa558a97a80955830d'] = 'Delicioso';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_f8e48612cec7fa70a41453379fdcc38a'] = 'Url delicioso';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_fb6644251aef5e594752733dda0165d1'] = 'Url completa da página deliciosa (deixe em branco para usar o compartilhamento em vez de um link para o site)';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_b632c55a33530d1433e29ffc09ba1151'] = 'Reddit';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_d381875976aaa9ddbccab5b05f399b49'] = 'Url do reddit';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_d2ac5aa86fc36a5f29777d8ab6a9f73e'] = 'Url completa da página Reddit (deixe em branco para usar o compartilhamento em vez de um link para o site)';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_814ad25b7a17f9feccd3dc98b7c17bdd'] = 'como \'@username\'. Use @ para o usuário e # para etiquetas, você pode usar vários usuários ou tags:\'@username\',\'@username2\'\'#tag1\'';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_b8aa4fe8cca87770e3ced327ac3ed250'] = 'Gerar aqui: https://smashballoon.com/custom-facebook-feed/access-token/';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_ffbd688bf9f0299d75c8abe975b16c6d'] = 'como \'@username\'. Use @ para o usuário e # para etiquetas, você pode usar vários usuários ou tags:\'@username\',\'@username2\'\'#tag1\'';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_c46526afaf8301c71b765234ae23dc8b'] = 'StumbleUpon';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_1334b6ec0ff0dc970481738a2374448c'] = 'Yahoo';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_a4cd3bb8d3f54f42a3bf40bc31d568ab'] = 'Yahoo url';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_6699ae5d84461afef2fbb70cbb315b17'] = 'Url completo de sua página do Yahoo (deixe em branco para usar o compartilhamento em vez de um link para o site)';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_9c393f1c3d00f6f6ab7f2f393508cf5d'] = 'Digg';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_3b8fc56e4c58accc518452c103b3990e'] = 'Url do Digg';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_603f00bf9e2fa7a32e87683ac328291f'] = 'Url completo de sua página do Digg (deixe em branco para usar o compartilhamento em vez de um link para o site)';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_3b0f3a25d07e5d9dbdf98db15ee70410'] = 'Flickr';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_44e2308cdc51a0a1a5350ba3937e0b5e'] = 'LinkedIn';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_1d14791ad69312ad279b09c9ac205879'] = 'Url do LinkedIn';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_41db961d0a03478919762ffb51bab6fb'] = 'Url completo de sua página de Linkedin (deixe em branco para usar o compartilhamento em vez de um link para o site)';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_b6ab63942d929d18f3feaac140abfdf8'] = 'como \'@+username\'. Use @ para o usuário e # para etiquetas, você pode usar vários usuários ou tags:\'@+username\',\'@+username2\'\'#tag1\'';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_f755c9473f48836636bafb0d1c979658'] = 'como \\\'&userid\\\'. Uso & para usuário e # para tags. Veja o seu Instagram simbólica e colar aqui para obter o seu ID de USUÁRIO https://api.instagram.com/v1/users/self/?access_token=xxxx, você pode usar vários usuários ou tags: \\\'&username\\\', \\\'&username2\\\'\\ \'#tag1\\\'';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_a264a691c1b240bac225d904cba98ab0'] = 'Obter o ID de cliente aqui: https://instagram.com/developer/clients/manage/. Depois disso, edite o cliente e na guia Segurança, desmarque desativar implícita OAuth';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_b4512dffcc7fb0cbbc7002e676cd3616'] = 'Veja o símbolo aqui: https://developers.pinterest.com/tools/access_token/';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_3f39588bb19e28051d9aedfbb170025c'] = 'Sombra';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_231e3a1801d1e19319ea4c09d87362eb'] = 'Sombra de elementos como: 0 1px 0 0 rgba (10, 10, 10, 0.2)';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_24560c17ce77a6fe44a237bb410781c5'] = 'Ocultar posts sagacidade hthis palavras (como: word1, word2, word3)';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_b78ca76f556513399068bb27497032d2'] = 'Você pode transplantar o módulo em colunas ou rodapé. Vá para módulos, um módulo de transplante e transplantá-la, mas o módulo pode ser apenas em um gancho no tempo.';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_f6249fb581179eb63a11aa5ac1914f43'] = 'O módulo tem uma página própria para mostrar a vapor, você pode acessar com o url http://yourite/modules/prestasocialstream/prestasocialstream-page.php.';
$_MODULE['<{prestasocialstream}prestashop>prestasocialstream_0c543c2a6d189444d0c50a964c22d58a'] = 'Você pode criar caixa códigos de sombra nesse URL:';
