<?php
/**
 * 2007-2014 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * No redistribute in other sites, or copy.
 *
 * @author    RSI <demo@demo.com>
 * @copyright 2007-2016 RSI
 * @license   http://localhost
 */

class PrestaSocialStreamDefaultModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        if ($this->context->getMobileDevice() == false) {
            // These hooks aren't used for the mobile theme.
            // Needed hooks are called in the tpl files.
            if (!isset($this->context->cart)) {
                $this->context->cart = new Cart();
            }
            $this->context->smarty->assign(
                array(
                    'HOOK_HEADER' => Hook::exec('displayHeader'),
                    'HOOK_TOP' => Hook::exec('displayTop'),
                    //	'HOOK_LEFT_COLUMN'  => ($this->display_column_left ? Hook::exec('displayLeftColumn') : ''),
                    //'HOOK_RIGHT_COLUMN' => ($this->display_column_right ? Hook::exec('displayRightColumn', array('cart' => $this->context->cart)) : ''),
                )
            );
        } else {
            $this->context->smarty->assign(
                array(
                    'HOOK_MOBILE_HEADER' => Hook::exec('displayMobileHeader'),
                )
            );
        }

        $widthcs = Configuration::get('SOCIALSTREAM_WIDTHCS');
        $width = Configuration::get('SOCIALSTREAM_WIDTH');
        $margin = Configuration::get('SOCIALSTREAM_MARGIN');
        $height = Configuration::get('SOCIALSTREAM_HEIGHT');
        $formatcs = Configuration::get('SOCIALSTREAM_FORMATCS');
        $float = Configuration::get('SOCIALSTREAM_FLOAT');
        $li1 = Configuration::get('SOCIALSTREAM_li1');
        $li2 = Configuration::get('SOCIALSTREAM_li2');
        $li3 = Configuration::get('SOCIALSTREAM_li3');
        $li4 = Configuration::get('SOCIALSTREAM_li4');
        $li5 = Configuration::get('SOCIALSTREAM_li5');
        $li6 = Configuration::get('SOCIALSTREAM_li6');

        $c1 = Configuration::get('SOCIALSTREAM_c1');
        $c2 = Configuration::get('SOCIALSTREAM_c2');
        $c3 = Configuration::get('SOCIALSTREAM_c3');
        $c4 = Configuration::get('SOCIALSTREAM_c4');
        $c5 = Configuration::get('SOCIALSTREAM_c5');
        $c6 = Configuration::get('SOCIALSTREAM_c6');
        $c7 = Configuration::get('SOCIALSTREAM_c7');
        $c8 = Configuration::get('SOCIALSTREAM_c8');
        $c9 = Configuration::get('SOCIALSTREAM_c9');
        $c10 = Configuration::get('SOCIALSTREAM_c10');
        $c11 = Configuration::get('SOCIALSTREAM_c11');
        $c12 = Configuration::get('SOCIALSTREAM_c12');
        $c13 = Configuration::get('SOCIALSTREAM_c13');
        $c14 = Configuration::get('SOCIALSTREAM_c14');

        $c1t = Configuration::get('SOCIALSTREAM_c1t');
        $c2t = Configuration::get('SOCIALSTREAM_c2t');
        $c3t = Configuration::get('SOCIALSTREAM_c3t');
        $c4t = Configuration::get('SOCIALSTREAM_c4t');
        $c5t = Configuration::get('SOCIALSTREAM_c5t');
        $c6t = Configuration::get('SOCIALSTREAM_c6t');
        $c7t = Configuration::get('SOCIALSTREAM_c7t');
        $c8t = Configuration::get('SOCIALSTREAM_c8t');
        $c9t = Configuration::get('SOCIALSTREAM_c9t');
        $c10t = Configuration::get('SOCIALSTREAM_c10t');
        $c11t = Configuration::get('SOCIALSTREAM_c11t');
        $c12t = Configuration::get('SOCIALSTREAM_c12t');
        $c13t = Configuration::get('SOCIALSTREAM_c13t');
        $colorize = Configuration::get('SOCIALSTREAM_COLORIZE');
        $servername = $_SERVER['SERVER_NAME'];
        $requesturi = $_SERVER['REQUEST_URI'];
        $url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
        $filea = dirname($url);
        $this->context->smarty->assign(
            array(
                'psversion' => _PS_VERSION_,

            )
        );
        $this->setTemplate('soc2.tpl');
    }
}
