<?php
/**
 * 2007-2014 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * No redistribute in other sites, or copy.
 *
 * @author    shacker RSI
 * @copyright 2007-2014 RSI
 * @license   http://localhost
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class PrestaSocialStream extends Module
{
    public function __construct()
    {
        $this->name = 'prestasocialstream';
        $this->module_key = '9e7ac70be889e8eb22389ef228ceee6b';
        if (_PS_VERSION_ < '1.4.0.0') {
            $this->tab = 'Blocks';
        }
        if (_PS_VERSION_ > '1.4.0.0' && _PS_VERSION_ < '1.5.0.0') {
            $this->tab = 'front_office_features';
        }
        if (_PS_VERSION_ > '1.5.0.0') {
            $this->tab = 'social_networks';
        }

        if (_PS_VERSION_ > '1.6.0.0') {
            $this->bootstrap = true;
        }
        $this->version = '2.0.1';
        $this->author = 'RSI';
        $this->need_instance = 0;
        parent::__construct();
        $this->error = false;
        $this->valid = false;

        $this->displayName = $this->l('Prestashop Social Stream');
        $this->description = $this->l('A stream of social networks');
        if (_PS_VERSION_ < '1.5') {
            require(_PS_MODULE_DIR_.$this->name.'/backward_compatibility/backward.php');
        }
    }

    public function install()
    {
        if (parent::install() == false || $this->registerHook('header') == false || !$this->registerHook('home')) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_WIDTHCS',
            '150'
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_FORMATCS',
            ''
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_FLOAT',
            '1'
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_MARGIN',
            '0px'
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c1',
            '1'
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c1t',
            ''
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c2t',
            ''
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c3t',
            ''
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c4t',
            '\'@prestashop\''
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c5t',
            '\'@prestashop\''
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c6t',
            ''
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_BAD',
            'badwordshere,badwords2'
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_li1',
            '2'
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_li2',
            '2'
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_li3',
            '2'
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_li6',
            '2'
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_li5',
            '2'
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_li5',
            '2'
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c7t',
            ''
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c8t',
            ''
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c9t',
            ''
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c10t',
            ''
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c11t',
            ''
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c12t',
            ''
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c13t',
            '5f89addfe56f469791e5bfedfa6e178a'
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c14t',
            'AWHCR_IBRdXTDppdjelnlr8dxjBwFFVQJhBUnE5DCYA4V-AtlgAAAAA'
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c2',
            '1'
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c3',
            '1'
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c4',
            '150849908413827|a20e87978f1ac491a0c4a721c961b68c'
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c6',
            '1'
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c7',
            '1'
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c8',
            '1'
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c9',
            '1'
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c10',
            '\'@tesla\''
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c11',
            '0'
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c12',
            '\'@+prestashop\''
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c13',
            '1'
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c121',
            'AIzaSyCwf0p7o8S1hinwR6KqYqkAvikXwyb_Be8'
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c122',
            '507084886.d8d1d50.f198c2c771b74afc97cf9c06a2e59585'
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c123',
            'AWHCR_IBRdXTDppdjelnlr8dxjBwFFVQJhBUnE5DCYA4V-AtlgAAAAA'
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c13',
            '\'@prestashop\''
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c14',
            '\'prestashop/prestashopday-2015\''
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_SHADOW',
            '0 0 5px 0 rgba(10, 10, 10, 0.2)'
        )
        ) {
            return false;
        }

        if (!Configuration::updateValue(
            'SOCIALSTREAM_c5',
            'Eoa0uD4mtpyIgPQK0ALYQ'
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_c51',
            'jmPEG050W8Orf8tBgCRJGkilxTWWJHpZUX9BPqDgk'
        )
        ) {
            return false;
        }

        if (!Configuration::updateValue(
            'SOCIALSTREAM_WIDTH',
            '300'
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_HEIGHT',
            '000000'
        )
        ) {
            return false;
        }
        if (!Configuration::updateValue(
            'SOCIALSTREAM_COLORIZE',
            '0'
        )
        ) {
            return false;
        }

        return true;
    }

    public function postProcess()
    {
        $errors = '';
        $output = '';
        if (Tools::isSubmit('submitCoolshare')) {
            if ($widthcs = Tools::getValue('widthcs')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_WIDTHCS',
                    $widthcs
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_WIDTHCS');
            }

            if ($formatcs = Tools::getValue('formatcs')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_FORMATCS',
                    $formatcs
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_FORMATCS');
            }

            if ($float = Tools::getValue('float')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_FLOAT',
                    $float
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_FLOAT');
            }

            if ($bad = Tools::getValue('bad')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_BAD',
                    $bad
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_BAD');
            }
            if ($margin = Tools::getValue('margin')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_MARGIN',
                    $margin
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_MARGIN');
            }

            if ($c1 = Tools::getValue('c1')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c1',
                    $c1
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c1');
            }

            if ($li1 = Tools::getValue('li1')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_li1',
                    $li1
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_li1');
            }
            if ($li2 = Tools::getValue('li2')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_li2',
                    $li2
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_li2');
            }

            if ($li3 = Tools::getValue('li3')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_li3',
                    $li3
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_li3');
            }

            if ($li4 = Tools::getValue('li4')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_li4',
                    $li4
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_li4');
            }

            if ($li5 = Tools::getValue('li5')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_li5',
                    $li5
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_li5');
            }
            if ($li6 = Tools::getValue('li6')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_li6',
                    $li6
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_li6');
            }
            if ($c2 = Tools::getValue('c2')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c2',
                    $c2
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c2');
            }

            if ($c3 = Tools::getValue('c3')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c3',
                    $c3
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c3');
            }

            if ($c4 = Tools::getValue('c4')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c4',
                    $c4
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c4');
            }
            if ($c5 = Tools::getValue('c5')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c5',
                    $c5
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c5');
            }
            if ($c51 = Tools::getValue('c51')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c51',
                    $c51
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c51');
            }
            if ($c6 = Tools::getValue('c6')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c6',
                    $c6
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c6');
            }

            if ($c7 = Tools::getValue('c7')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c7',
                    $c7
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c7');
            }

            if ($c8 = Tools::getValue('c8')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c7',
                    $c8
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c7');
            }

            if ($c9 = Tools::getValue('c9')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c9',
                    $c9
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c9');
            }

            if ($c10 = Tools::getValue('c10')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c10',
                    $c10
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c10');
            }

            if ($c11 = Tools::getValue('c11')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c11',
                    $c11
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c11');
            }

            if ($c12 = Tools::getValue('c12')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c12',
                    $c12
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c12');
            }

            if ($c121 = Tools::getValue('c121')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c121',
                    $c121
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c121');
            }

            if ($c123 = Tools::getValue('c123')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c123',
                    $c123
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c123');
            }
            if ($c122 = Tools::getValue('c122')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c122',
                    $c122
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c122');
            }
            if ($c12 = Tools::getValue('c12')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c12',
                    $c12
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c12');
            }

            if ($c1t = Tools::getValue('c1t')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c1t',
                    $c1t
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c1t');
            }

            if ($c2t = Tools::getValue('c2t')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c2t',
                    $c2t
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c2t');
            }

            if ($c3t = Tools::getValue('c3t')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c3t',
                    $c3t
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c3t');
            }

            if ($c4t = Tools::getValue('c4t')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c4t',
                    $c4t
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c4t');
            }
            if ($c5t = Tools::getValue('c5t')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c5t',
                    $c5t
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c5t');
            }

            if ($c6t = Tools::getValue('c6t')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c6t',
                    $c6t
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c6t');
            }

            if ($c7t = Tools::getValue('c7t')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c7t',
                    $c7t
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c7t');
            }

            if ($c8t = Tools::getValue('c8t')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c7t',
                    $c8t
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c7t');
            }

            if ($c9t = Tools::getValue('c9t')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c9t',
                    $c9t
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c9t');
            }

            if ($c10t = Tools::getValue('c10t')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c10t',
                    $c10t
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c10t');
            }

            if ($c11t = Tools::getValue('c11t')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c11t',
                    $c11t
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c11t');
            }

            if ($c12t = Tools::getValue('c12t')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c12t',
                    $c12t
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c12t');
            }

            if ($c13t = Tools::getValue('c13t')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c13t',
                    $c13t
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c13t');
            }

            if ($c13 = Tools::getValue('c13')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c13',
                    $c13
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c13');
            }

            if ($color6 = Tools::getValue('color6')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_COLOR6',
                    $color6
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_COLOR6');
            }
            if ($c14 = Tools::getValue('c14')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_c14',
                    $c14
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_c14');
            }


            if ($width = Tools::getValue('width')) {
                Configuration::updateValue(
                    'PRESTAPROTECT_WIDHT',
                    $width
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_WIDHT');
            }

            if ($height = Tools::getValue('height')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_HEIGHT',
                    $height
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_HEIGHT');
            }

            if ($colorize = Tools::getValue('colorize')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_COLORIZE',
                    $colorize
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_COLORIZE');
            }


            if ($shadow = Tools::getValue('shadow')) {
                Configuration::updateValue(
                    'SOCIALSTREAM_SHADOW',
                    $shadow
                );
            } elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP) {
                Configuration::deleteFromContext('SOCIALSTREAM_SHADOW');
            }

            $this->writeCss(
                $colorize,
                $shadow
            );
            $output .= $this->displayConfirmation($this->l('Settings updated').'<br/>');

            if (!$errors) {
                return $output;
            }
        }
    }

    public function writeCss(
        $colorize,
        $shadow
    ) {
        $xml2 = fopen(
            _PS_ROOT_DIR_.'/modules/prestasocialstream/views/css/'.((_PS_VERSION_ > '1.5.0.0') ? $this->context->shop->id : '').'jquery.socialfeed.css',
            'w'
        );
        fwrite(
            $xml2,
            '
/* Plugin styles */

.social-feed-container{width:100%; position:relative; float:left; clear:both}
.social-feed-element.hidden{
background-color: white !important;
    visibility: visible!important;
    display: inline-block!important;}
.social-feed-element .pull-left{
    float:left;
    margin-right: 10px;
}
.social-feed-element .pull-right {
    margin-left: 10px;
}
.social-feed-element img {
    width: 100%;
    width: auto\9;
    height: auto;
    border: 0;  
    vertical-align: middle;
    -ms-interpolation-mode: bicubic;
}
.social-feed-element .attachment {
    vertical-align: middle;
    -ms-interpolation-mode: bicubic;
}

/* Link styles */
.social-feed-element a {
    color: #0088cc;
    text-decoration: none;
}
.social-feed-element a:focus {
    outline: thin dotted #333;
    outline: 5px auto -webkit-focus-ring-color;
    outline-offset: -2px;
}
.social-feed-element a:hover,
.social-feed-element a:active {
    outline: 0;
    color: #005580;
    text-decoration: underline;
}

/* Text styles */
.social-feed-element small {
    font-size: 85%;
}
.social-feed-element strong {
    font-weight: bold;
}
.social-feed-element em {
    font-style: italic;
}
.social-feed-element p {
    margin: 0 0 10px;
}
.social-feed-element .media-body > p{
    margin-bottom:4px;
    min-height:20px;
}


/* Message styles */
.social-feed-element,
.social-feed-element .media-body {
    overflow: hidden;
    zoom: 1;
    *overflow: visible;
}
.social-feed-element .media-body .social-network-icon{
    margin-top: -3px;
    margin-right:5px;
    width:16px;
}
.social-feed-element .media-body div{
    color:#666;
    line-height: 20px;
}
.social-feed-element:first-child {

}
.social-feed-element .media-object {
    display: block;
    width:48px;
    border-radius:50%;
}
.social-feed-element .media-heading {
    margin: 0 0 5px;
}
.social-feed-element .media-list {
    margin-left: 0;
    list-style: none;
}

.social-feed-element .muted {
    color: #999;
}
.social-feed-element a.muted:hover,
.social-feed-element a.muted:focus {
    color: #808080;
}

.social-feed-element{
    box-shadow:'.$shadow.';
    transition: 0.25s;
    -webkit-backface-visibility: hidden;
    margin:-1px;
    margin-top:25px;
    background-color: #fff;
    color: #333;
    text-align:left;
    font-size: 14px;
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    line-height: 16px; 
	/*width:48%; float: left; margin:1%*/
	
}
.social-feed-element:hover{
    box-shadow: 0 0 20px 0 rgba(10, 10, 10, 0.4);
}

.social-feed-element .content{
    padding:15px;
}
.social-feed-element .social-network-icon{
    opacity:0.7;
}


.social-feed-element .author-title{
    color: #444;
    line-height: 1.5em;
    font-weight: 500;  
}
#containersf {
    width: 100%;
    margin: 0 auto;
}
 
.social-feed-container {
    -moz-column-count: '.$colorize.';
    -moz-column-gap: 0px;
    -webkit-column-count: '.$colorize.';
    -webkit-column-gap: 0px;
    column-count: '.$colorize.';
    column-gap: 0px;
    width: 100%;
}
#left_column .social-feed-container,#right_column .social-feed-container {
    -moz-column-count: 1;
    -moz-column-gap: 0px;
    -webkit-column-count: 1;
    -webkit-column-gap: 0px;
    column-count: 1;
    column-gap: 0px;
    width: 100%; float:inherit
}
#left_column .social-feed-element .muted,#right_column .social-feed-element .muted{font-size: 10px;
  right: 8px;  position: absolute;}
#left_column  .social-feed-element .media-object, #right_column .social-feed-element .media-object{width: 24px;}
.social-feed-container .social-feed-element {
    display: inline-block;
    margin-bottom: 0px;
    width: 96%;
	margin:2%
}
@media screen and (min-width:100px) and (max-width:680px) {
.social-feed-container {
    -moz-column-count: 1;
    -moz-column-gap: 0px;
    -webkit-column-count: 1;
    -webkit-column-gap: 0px;
    column-count: 1;
    column-gap: 0px;
    width: 100%;
}	
}
@media screen and (min-width:1200px){

.social-feed-element p.social-feed-text {
    margin: 0;
    overflow: hidden;
    text-overflow: ellipsis;
    -webkit-line-clamp: 5;
    -webkit-box-orient: vertical;
}
}

        '
        );
    }

    public function getConfigFieldsValues()
    {
        $fields_values = array(
            'widthcs' => Tools::getValue(
                'widthcs',
                Configuration::get('SOCIALSTREAM_WIDTHCS')
            ),
            'colorize' => Tools::getValue(
                'colorize',
                Configuration::get('SOCIALSTREAM_COLORIZE')
            ),
            'width' => Tools::getValue(
                'width',
                Configuration::get('SOCIALSTREAM_WIDTH')
            ),
            'margin' => Tools::getValue(
                'margin',
                Configuration::get('SOCIALSTREAM_MARGIN')
            ),
            'formatcs' => Tools::getValue(
                'formatcs',
                Configuration::get('SOCIALSTREAM_FORMATCS')
            ),
            'float' => Tools::getValue(
                'float',
                Configuration::get('SOCIALSTREAM_FLOAT')
            ),
            'bad' => Tools::getValue(
                'bad',
                Configuration::get('SOCIALSTREAM_FLOAT')
            ),
            'height' => Tools::getValue(
                'height',
                Configuration::get('SOCIALSTREAM_BAD')
            ),
            'c1' => Tools::getValue(
                'c1',
                Configuration::get('SOCIALSTREAM_c1')
            ),
            'shadow' => Tools::getValue(
                'shadow',
                Configuration::get('SOCIALSTREAM_SHADOW')
            ),
            'c2' => Tools::getValue(
                'c2',
                Configuration::get('SOCIALSTREAM_c2')
            ),
            'c3' => Tools::getValue(
                'c3',
                Configuration::get('SOCIALSTREAM_c3')
            ),
            'c4' => Tools::getValue(
                'c4',
                Configuration::get('SOCIALSTREAM_c4')
            ),
            'li1' => Tools::getValue(
                'li1',
                Configuration::get('SOCIALSTREAM_li1')
            ),
            'li2' => Tools::getValue(
                'li2',
                Configuration::get('SOCIALSTREAM_li2')
            ),
            'li3' => Tools::getValue(
                'li3',
                Configuration::get('SOCIALSTREAM_li3')
            ),
            'li4' => Tools::getValue(
                'li4',
                Configuration::get('SOCIALSTREAM_li4')
            ),
            'li5' => Tools::getValue(
                'li5',
                Configuration::get('SOCIALSTREAM_li5')
            ),
            'li6' => Tools::getValue(
                'li6',
                Configuration::get('SOCIALSTREAM_li6')
            ),
            'c5' => Tools::getValue(
                'c5',
                Configuration::get('SOCIALSTREAM_c5')
            ),
            'c51' => Tools::getValue(
                'c51',
                Configuration::get('SOCIALSTREAM_c51')
            ),
            'c6' => Tools::getValue(
                'c6',
                Configuration::get('SOCIALSTREAM_c6')
            ),
            'c7' => Tools::getValue(
                'c7',
                Configuration::get('SOCIALSTREAM_c7')
            ),
            'c8' => Tools::getValue(
                'c8',
                Configuration::get('SOCIALSTREAM_c8')
            ),
            'c9' => Tools::getValue(
                'c9',
                Configuration::get('SOCIALSTREAM_c9')
            ),
            'c10' => Tools::getValue(
                'c10',
                Configuration::get('SOCIALSTREAM_c10')
            ),
            'c11' => Tools::getValue(
                'c11',
                Configuration::get('SOCIALSTREAM_c11')
            ),
            'c12' => Tools::getValue(
                'c12',
                Configuration::get('SOCIALSTREAM_c12')
            ),
            'c121' => Tools::getValue(
                'c121',
                Configuration::get('SOCIALSTREAM_c121')
            ),
            'c122' => Tools::getValue(
                'c122',
                Configuration::get('SOCIALSTREAM_c122')
            ),
            'c123' => Tools::getValue(
                'c123',
                Configuration::get('SOCIALSTREAM_c123')
            ),
            'c13' => Tools::getValue(
                'c13',
                Configuration::get('SOCIALSTREAM_c13')
            ),
            'c14' => Tools::getValue(
                'c14',
                Configuration::get('SOCIALSTREAM_c14')
            ),
            'c1t' => Tools::getValue(
                'c1t',
                Configuration::get('SOCIALSTREAM_c1t')
            ),
            'c2t' => Tools::getValue(
                'c2t',
                Configuration::get('SOCIALSTREAM_c2t')
            ),
            'c3t' => Tools::getValue(
                'c3t',
                Configuration::get('SOCIALSTREAM_c3t')
            ),
            'c4t' => Tools::getValue(
                'c4t',
                Configuration::get('SOCIALSTREAM_c4t')
            ),
            'c5t' => Tools::getValue(
                'c5t',
                Configuration::get('SOCIALSTREAM_c5t')
            ),
            'c6t' => Tools::getValue(
                'c6t',
                Configuration::get('SOCIALSTREAM_c6t')
            ),
            'c7t' => Tools::getValue(
                'c7t',
                Configuration::get('SOCIALSTREAM_c7t')
            ),
            'c8t' => Tools::getValue(
                'c8t',
                Configuration::get('SOCIALSTREAM_c8t')
            ),
            'c9t' => Tools::getValue(
                'c9t',
                Configuration::get('SOCIALSTREAM_c9t')
            ),
            'c10t' => Tools::getValue(
                'c10t',
                Configuration::get('SOCIALSTREAM_c10t')
            ),
            'c11t' => Tools::getValue(
                'c11t',
                Configuration::get('SOCIALSTREAM_c11t')
            ),
            'c12t' => Tools::getValue(
                'c12t',
                Configuration::get('SOCIALSTREAM_c12t')
            ),
            'c13t' => Tools::getValue(
                'c13t',
                Configuration::get('SOCIALSTREAM_c13t')
            ),
        );
        return $fields_values;
    }

    public function renderForm()
    {
        $options1 = array(
            array(
                'id_option' => '1',
                // The value of the 'value' attribute of the <option> tag.
                'name' => '1'
                // The value of the text content of the  <option> tag.
            ),
            array(
                'id_option' => '2',
                'name' => '2'
            ),
            array(
                'id_option' => '3',
                'name' => '3'
            ),

        );
        $options2 = array(
            array(
                'id_option' => '2',
                // The value of the 'value' attribute of the <option> tag.
                'name' => 'Top right'
                // The value of the text content of the  <option> tag.
            ),
            array(
                'id_option' => '1',
                'name' => 'Top left'
            ),
            array(
                'id_option' => '3',
                'name' => 'Bottom right'
            ),
            array(
                'id_option' => '4',
                'name' => 'Bottom left'
            ),

        );

        $this->postProcess();
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Configuration'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Text lenght'),
                        'name' => 'widthcs',
                        'desc' => $this->l('The total lenght of text for every post.'),

                    ),
                    /*	array(
                            'type'  => 'color',
                            'name'  => 'height',
                            'label' => $this->l('Background color'),
                            'desc'  => $this->l('Color of tab (default ffffff)'),

                        ),*/

                    array(
                        'type' => 'select',
                        'label' => $this->l('Columns'),
                        'name' => 'colorize',
                        'desc' => $this->l('Columns to show'),
                        'options' => array(
                            'query' => $options1,
                            'id' => 'id_option',
                            'name' => 'name'
                        )
                    ),
                    /*	array(
                            'type'    => 'select',
                            'label'   => $this->l('Position'),
                            'name'    => 'float',
                            'options' => array(
                                'query' => $options2,
                                'id'    => 'id_option',
                                'name'  => 'name'
                            )
                        ),*/
                    array(
                        'type' => 'text',
                        'label' => $this->l('Facebook user'),
                        'name' => 'c4t',
                        'desc' => $this->l(
                            'like \'@username\'. Use @ for user and # for tags, you can use various users or tags:\'@username\',\'@username2\'\'#tag1\' '
                        ),

                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Post to show'),
                        'name' => 'li1',
                        'desc' => $this->l('Total of post to show'),

                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Facebook Access token'),
                        'name' => 'c4',
                        'desc' => $this->l(
                            'Generate here: https://smashballoon.com/custom-facebook-feed/access-token/'
                        ),

                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Twitter user'),
                        'name' => 'c5t',
                        'desc' => $this->l(
                            'like \'@username\'. Use @ for user and # for tags, you can use various users or tags:\'@username\',\'@username2\'\'#tag1\' '
                        ),

                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Post to show'),
                        'name' => 'li2',
                        'desc' => $this->l('Total of post to show'),

                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Twitter consumer key'),
                        'name' => 'c5',
                        'desc' => $this->l('Create an app here to get the values https://apps.twitter.com/'),

                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Twitter consumer secret'),
                        'name' => 'c51',
                        'desc' => $this->l('Create an app here to get the values https://apps.twitter.com/'),

                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('VK user'),
                        'name' => 'c10',
                        'desc' => $this->l(
                            'like \'@username\'. Use @ for user and # for tags, you can use various users or tags:\'@username\',\'@username2\'\'#tag1\' '
                        ),

                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Post to show'),
                        'name' => 'li3',
                        'desc' => $this->l('Total of post to show'),

                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Google+'),
                        'name' => 'c12',
                        'desc' => $this->l(
                            'like \'@+username\'. Use @ for user and # for tags, you can use various users or tags:\'@+username\',\'@+username2\'\'#tag1\' '
                        ),

                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Post to show'),
                        'name' => 'li4',
                        'desc' => $this->l('Total of post to show'),

                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Google Access token'),
                        'name' => 'c121',
                        'desc' => $this->l('Get google token from: https://console.developers.google.com'),

                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Instagram user ID'),
                        'name' => 'c13',
                        'desc' => $this->l(
                            'like \'&userid\'. Use & for user and # for tags. Get your Instagram access token, and paste here to get your USER ID https://api.instagram.com/v1/users/self/?access_token=xxxx, you can use various users or tags:\'&username\',\'&username2\'\'#tag1\' '
                        ),

                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Post to show'),
                        'name' => 'li5',
                        'desc' => $this->l('Total of post to show'),

                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Instagram client ID'),
                        'name' => 'c13t',
                        'desc' => $this->l(
                            'Get the client ID here (create a new one): https://instagram.com/developer/clients/manage/. Edit the client created and in the seccurity tab, unselect the option Disable implicit OAuth'
                        ),

                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Instagram access token'),
                        'name' => 'c122',
                        'desc' => $this->l('Get the access token here: http://instagram.pixelunion.net/'),

                    ),
                    /*pinterest*/
                    array(
                        'type' => 'text',
                        'label' => $this->l('Pinterest user'),
                        'name' => 'c14',
                        'desc' => $this->l('like \'@username/boardname\'. Use @ for user and # for tags'),

                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Post to show'),
                        'name' => 'li6',
                        'desc' => $this->l('Total of post to show'),

                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Pinterest access token'),
                        'name' => 'c123',
                        'desc' => $this->l(
                            'Get the access token here: https://developers.pinterest.com/tools/access_token/'
                        ),

                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Show Media'),
                        'name' => 'c11',
                        'values' => array(
                            array(
                                'id' => 'on',
                                'value' => 1
                            ),
                            array(
                                'id' => 'off',
                                'value' => 0
                            )
                        )
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Box shadow'),
                        'name' => 'shadow',
                        'desc' => $this->l('Type of shadow like: 0 0 1px 0 rgba(10, 10, 10, 0.2)'),

                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Bad words'),
                        'name' => 'bad',
                        'desc' => $this->l('Not dispplay posts with these words (like: word1,word2,word3,)'),

                    ),

                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                )

            ),

        );
        $helper = new HelperForm();
        $helper->show_toolbar = true;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get(
            'PS_BO_ALLOW_EMPLOYEE_FORM_LANG'
        ) : 0;
        $this->fields_form = array();
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitCoolshare';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );
        return $helper->generateForm(array($fields_form));
    }

    public function getContent()
    {
        $errors = '';
        if (_PS_VERSION_ < '1.6.0.0') {
            $output = '<h2>'.$this->displayName.'</h2>';
            if (Tools::isSubmit('submitCoolshare')) {
                $widthcs = Tools::getValue('widthcs');
                $formatcs = Tools::getValue('formatcs');
                $float = Tools::getValue('float');
                $shadow = Tools::getValue('shadow');
                $margin = Tools::getValue('margin');
                $li1 = Tools::getValue('li1');
                $li2 = Tools::getValue('li2');
                $li3 = Tools::getValue('li3');
                $li4 = Tools::getValue('li4');
                $li5 = Tools::getValue('li5');
                $li6 = Tools::getValue('li6');
                $c1 = Tools::getValue('c1');
                $c121 = Tools::getValue('c121');
                $c122 = Tools::getValue('c122');
                $c123 = Tools::getValue('c123');
                $c1t = Tools::getValue('c1t');
                $c2t = Tools::getValue('c2t');
                $c3t = Tools::getValue('c3t');
                $c4t = Tools::getValue('c4t');
                $c5t = Tools::getValue('c5t');
                $c6t = Tools::getValue('c6t');
                $c7t = Tools::getValue('c7t');
                $c8t = Tools::getValue('c8t');
                $c9t = Tools::getValue('c9t');
                $c10t = Tools::getValue('c10t');
                $c11t = Tools::getValue('c11t');
                $c12t = Tools::getValue('c12t');
                $c13t = Tools::getValue('c13t');
                $c2 = Tools::getValue('c2');
                $c3 = Tools::getValue('c3');
                $c4 = Tools::getValue('c4');
                $c5 = Tools::getValue('c5');
                $bad = Tools::getValue('bad');
                $c51 = Tools::getValue('c51');
                $c6 = Tools::getValue('c6');
                $c7 = Tools::getValue('c7');
                $c8 = Tools::getValue('c8');
                $c9 = Tools::getValue('c9');
                $c10 = Tools::getValue('c10');
                $c11 = Tools::getValue('c11');
                $c12 = Tools::getValue('c12');
                $c13 = Tools::getValue('c13');
                $c14 = Tools::getValue('c14');
                $width = Tools::getValue('width');
                $height = Tools::getValue('height');
                $colorize = Tools::getValue('colorize');
                Configuration::updateValue(
                    'SOCIALSTREAM_WIDTHCS',
                    $widthcs
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_COLORIZE',
                    $colorize
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_WIDTH',
                    $width
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_BAD',
                    $bad
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_MARGIN',
                    $margin
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_HEIGHT',
                    $height
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_FORMATCS',
                    $formatcs
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_FLOAT',
                    $float
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_li1',
                    $li1
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_li2',
                    $li2
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_li3',
                    $li3
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_li4',
                    $li4
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_SHADOW',
                    $shadow
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_li5',
                    $li5
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_li6',
                    $li6
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_c1',
                    $c1
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_c121',
                    $c121
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_c123',
                    $c123
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_c122',
                    $c122
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_c2',
                    $c2
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_c3',
                    $c3
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_c4',
                    $c4
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_c5',
                    $c5
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_c51',
                    $c51
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_c6',
                    $c6
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_c7',
                    $c7
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_c8',
                    $c8
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_c9',
                    $c9
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_c10',
                    $c10
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_c11',
                    $c11
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_c12',
                    $c12
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_c13',
                    $c13
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_c14',
                    $c14
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_c1t',
                    $c1t
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_c2t',
                    $c2t
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_c3t',
                    $c3t
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_c4t',
                    $c4t
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_c5t',
                    $c5t
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_c6t',
                    $c6t
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_c7t',
                    $c7t
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_c8t',
                    $c8t
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_c9t',
                    $c9t
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_c10t',
                    $c10t
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_c11t',
                    $c11t
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_c12t',
                    $c12t
                );
                Configuration::updateValue(
                    'SOCIALSTREAM_c13t',
                    $c13t
                );
                if ($errors) {
                    $output .= $this->displayError(
                        implode(
                            '<br />',
                            $errors
                        )
                    );
                } else {
                    $output .= $this->displayConfirmation($this->l('Settings updated'));
                }
            }
            $this->writeCss(
                @$colorize,
                @$shadow
            );
            return $output.$this->displayForm();
        } else {
            return $this->postProcess().$this->_displayInfo().$this->renderForm().$this->_displayAdds();
        }

    }

    public function displayForm()
    {
        $output = '
	<link rel="stylesheet" href="../modules/prestasocialstream/views/css/colorpicker.css" type="text/css" />
	
		<script type="text/javascript" src="../modules/prestasocialstream/views/js/colorpicker.js"></script>
		<script type="text/javascript" src="../modules/prestasocialstream/views/js/eye.js"></script>
		<script type="text/javascript" src="../modules/prestasocialstream/views/js/utils.js"></script>
		<script type="text/javascript" src="../modules/prestasocialstream/views/js/layout.js?ver=1.0.2"></script>
		<form action="'.$_SERVER['REQUEST_URI'].'" method="post">
			<fieldset><legend><img src="'.$this->_path.'logo.gif" alt="" title="" />'.$this->l('Settings').'</legend>
	
	<label>'.$this->l('Text lenght').'</label>
				<div class="margin-form">
					<input type="text" size="5" name="widthcs" value="'.Tools::getValue('widthcs', Configuration::get('SOCIALSTREAM_WIDTHCS')).'" />
					<p class="clear">'.$this->l('The total lenght of text for every post.').'</p>
					
				</div>
			<!--	<label>'.$this->l('Animation speed').'</label>
				<div class="margin-form">
					<input type="text" size="5" name="width" value="'.Tools::getValue('width', Configuration::get('SOCIALSTREAM_WIDTH')).'" />
					<p class="clear">'.$this->l('Speed of the animation (200 default)').'</p>
					
				</div>
							<label>'.$this->l('Background color').'</label>
				<div class="margin-form">

					<div id="customWidget">
						<input type="text" maxlength="6"  name="height" size="6" id="colorpickerField1" value="'.str_replace('#', '', Tools::getValue('height', Configuration::get('SOCIALSTREAM_HEIGHT'))).'" />	<p class="clear">'.$this->l('Color of tab (default ffffff)').'</p>
					
					</div>
					</div> 
				<label>'.$this->l('Margin from top or bottom').'</label>
				<div class="margin-form">
					<input type="text" size="5" name="margin" value="'.Tools::getValue('margin', Configuration::get('SOCIALSTREAM_MARGIN')).'" />
				
					
				</div>-->
					<label>'.$this->l('Columns').'</label>
	<div class="margin-form">
  <select name="colorize" >
	  <option value="1"'.((Configuration::get('SOCIALSTREAM_COLORIZE') == '1') ? 'selected="selected"' : '').'>1</option>
	    <option value="2"'.((Configuration::get('SOCIALSTREAM_COLORIZE') == '2') ? 'selected="selected"' : '').'>2</option>
			    <option value="3"'.((Configuration::get('SOCIALSTREAM_COLORIZE') == '3') ? 'selected="selected"' : '').'>3</option>

    </select>
		<p>'.$this->l('Columns to show').'</p>
	</div>
					<!--<label>'.$this->l('Image format').'</label>
	<div class="margin-form">
  <select name="formatcs" >
  <option value=""'.((Configuration::get('SOCIALSTREAM_FORMATCS') == '') ? 'selected="selected"' : '').'>Transparent</option>
	  <option value="2"'.((Configuration::get('SOCIALSTREAM_FORMATCS') == '2') ? 'selected="selected"' : '').'>Grayscale</option>
	
    </select>
	</div> 
	
		<label>'.$this->l('Position').'</label>
	<div class="margin-form">
  <select name="float" >
  <option value="2"'.((Configuration::get('SOCIALSTREAM_FLOAT') == '2') ? 'selected="selected"' : '').'>Top right</option>
	  <option value="1"'.((Configuration::get('SOCIALSTREAM_FLOAT') == '1') ? 'selected="selected"' : '').'>Top left</option>
	   <option value="3"'.((Configuration::get('SOCIALSTREAM_FLOAT') == '3') ? 'selected="selected"' : '').'>Bottom right</option>
	    <option value="4"'.((Configuration::get('SOCIALSTREAM_FLOAT') == '4') ? 'selected="selected"' : '').'>Bottom left</option>
	
    </select>
	</div>-->
	<!--<label>'.$this->l('Technorati').'</label>
	<div class="margin-form">
  <select name="c1" >
  <option value="1"'.((Configuration::get('SOCIALSTREAM_c1') == '1') ? 'selected="selected"' : '').'>Yes</option>
	  <option value="0"'.((Configuration::get('SOCIALSTREAM_c1') == '0') ? 'selected="selected"' : '').'>No</option>
    </select>

		
		</div>
			<label>'.$this->l('Technorati url').'</label>
				<div class="margin-form">
					<input type="text" size="50" name="c1t" value="'.Tools::getValue('c1t', Configuration::get('SOCIALSTREAM_c1t')).'" />
					<p class="clear">'.$this->l('Full url of your Technorati page (leave blank to use share instead of a link to the site)').'</p>
					
				</div>
			<label>'.$this->l('Delicious').'</label>
	<div class="margin-form">
  <select name="c2" >
  <option value="1"'.((Configuration::get('SOCIALSTREAM_c2') == '1') ? 'selected="selected"' : '').'>Yes</option>
	  <option value="0"'.((Configuration::get('SOCIALSTREAM_c2') == '0') ? 'selected="selected"' : '').'>No</option>
    </select>
		</div>
				<label>'.$this->l('Delicious url').'</label>
				<div class="margin-form">
					<input type="text" size="50" name="c2t" value="'.Tools::getValue('c2t', Configuration::get('SOCIALSTREAM_c2t')).'" />
					<p class="clear">'.$this->l('Full url of your Delicious page (leave blank to use share instead of a link to the site)').'</p>
					
				</div>
			<label>'.$this->l('Reddit').'</label>
	<div class="margin-form">
  <select name="c3" >
  <option value="1"'.((Configuration::get('SOCIALSTREAM_c3') == '1') ? 'selected="selected"' : '').'>Yes</option>
	  <option value="0"'.((Configuration::get('SOCIALSTREAM_c3') == '0') ? 'selected="selected"' : '').'>No</option>
    </select>

		
		</div>
				<label>'.$this->l('Reddit url').'</label>
				<div class="margin-form">
					<input type="text" size="50" name="c3t" value="'.Tools::getValue('c3t', Configuration::get('SOCIALSTREAM_c3t')).'" />
					<p class="clear">'.$this->l('Full url of your Reddit page (leave blank to use share instead of a link to the site)').'</p>
					
				</div>
				-->
			
				<label>'.$this->l('Facebook user').'</label>
				<div class="margin-form">
					<input type="text" size="50" name="c4t" value="'.Tools::getValue('c4t', Configuration::get('SOCIALSTREAM_c4t')).'" />
					<p class="clear">'.$this->l('like \'@username\'. Use @ for user and # for tags, you can use various users or tags:\'@username\',\'@username2\'\'#tag1\' ').'</p>
					
				</div>
					<label>'.$this->l('Post to show').'</label>
				<div class="margin-form">
					<input type="text" size="50" name="li1" value="'.Tools::getValue('li1', Configuration::get('SOCIALSTREAM_li1')).'" />
					<p class="clear">'.$this->l('Total of post to show').'</p>
					
				</div>
				<label>'.$this->l('Facebook Access token').'</label>
				<div class="margin-form">
					<input type="text" size="50" name="c4" value="'.Tools::getValue('c4', Configuration::get('SOCIALSTREAM_c4')).'" />
					<p class="clear">'.$this->l('Generate here: https://smashballoon.com/custom-facebook-feed/access-token/').'</p>
					
				</div>
	
					<label>'.$this->l('Twitter user').'</label>
				<div class="margin-form">
					<input type="text" size="50" name="c5t" value="'.Tools::getValue('c5t', Configuration::get('SOCIALSTREAM_c5t')).'" />
					<p class="clear">'.$this->l('like \'@username\'. Use @ for user and # for tags, you can use various users or tags:\'@username\',\'@username2\'\'#tag1\'').'</p>
					
				</div>
				<label>'.$this->l('Post to show').'</label>
				<div class="margin-form">
					<input type="text" size="50" name="li2" value="'.Tools::getValue('li2', Configuration::get('SOCIALSTREAM_li2')).'" />
					<p class="clear">'.$this->l('Total of post to show').'</p>
					
				</div>
				<label>'.$this->l('Twitter consumer key').'</label>
				<div class="margin-form">
					<input type="text" size="50" name="c5" value="'.Tools::getValue('c5', Configuration::get('SOCIALSTREAM_c5')).'" />
					<p class="clear">'.$this->l('Create an app here to get the values https://apps.twitter.com/').'</p>
					
				</div>
				<label>'.$this->l('Twitter consumer secret').'</label>
				<div class="margin-form">
					<input type="text" size="50" name="c51" value="'.Tools::getValue('c51', Configuration::get('SOCIALSTREAM_c51')).'" />
					<p class="clear">'.$this->l('Create an app here to get the values https://apps.twitter.com/').'</p>
					
				</div>
			<!--	<label>'.$this->l('Stumbleupon').'</label>
	<div class="margin-form">
  <select name="c6" >
  <option value="1"'.((Configuration::get('SOCIALSTREAM_c6') == '1') ? 'selected="selected"' : '').'>Yes</option>
	  <option value="0"'.((Configuration::get('SOCIALSTREAM_c6') == '0') ? 'selected="selected"' : '').'>No</option>
    </select>
	
		
		</div>-->
					<label>'.$this->l('VK user').'</label>
				<div class="margin-form">
					<input type="text" size="50" name="c10" value="'.Tools::getValue('c10', Configuration::get('SOCIALSTREAM_c10')).'" />
					<p class="clear">'.$this->l('like \'@username\'. Use @ for user and # for tags, you can use various users or tags:\'@username\',\'@username2\'\'#tag1\'').'</p>
					
				</div>
				<label>'.$this->l('Post to show').'</label>
				<div class="margin-form">
					<input type="text" size="50" name="li3" value="'.Tools::getValue('li3', Configuration::get('SOCIALSTREAM_li3')).'" />
					<p class="clear">'.$this->l('Total of post to show').'</p>
					
				</div>
				<!--<label>'.$this->l('Yahoo').'</label>
	<div class="margin-form">
  <select name="c7" >
  <option value="1"'.((Configuration::get('SOCIALSTREAM_c7') == '1') ? 'selected="selected"' : '').'>Yes</option>
	  <option value="0"'.((Configuration::get('SOCIALSTREAM_c7') == '0') ? 'selected="selected"' : '').'>No</option>
    </select>

		
		</div>
					<label>'.$this->l('Yahoo url').'</label>
				<div class="margin-form">
					<input type="text" size="50" name="c7t" value="'.Tools::getValue('c7t', Configuration::get('SOCIALSTREAM_c7t')).'" />
					<p class="clear">'.$this->l('Full url of your Yahoo page (leave blank to use share instead of a link to the site)').'</p>
					
				</div>
				<label>'.$this->l('Digg').'</label>
	<div class="margin-form">
  <select name="c8" >
  <option value="1"'.((Configuration::get('SOCIALSTREAM_c8') == '1') ? 'selected="selected"' : '').'>Yes</option>
	  <option value="0"'.((Configuration::get('SOCIALSTREAM_c8') == '0') ? 'selected="selected"' : '').'>No</option>
    </select>

		
		</div> 
				<label>'.$this->l('Digg url').'</label>
				<div class="margin-form">
					<input type="text" size="50" name="c8t" value="'.Tools::getValue('c8t', Configuration::get('SOCIALSTREAM_c8t')).'" />
					<p class="clear">'.$this->l('Full url of your Digg page (leave blank to use share instead of a link to the site)').'</p>
					
				</div> -->
		<!--
					<label>'.$this->l('Flickr').'</label>
	<div class="margin-form">
  <select name="c9" >
  <option value="1"'.((Configuration::get('SOCIALSTREAM_c9') == '1') ? 'selected="selected"' : '').'>Yes</option>
	  <option value="0"'.((Configuration::get('SOCIALSTREAM_c9') == '0') ? 'selected="selected"' : '').'>No</option>
    </select>

		
		</div> -->
			<!--<label>'.$this->l('Linkedin').'</label>
	<div class="margin-form">
  <select name="c10" >
  <option value="1"'.((Configuration::get('SOCIALSTREAM_c10') == '1') ? 'selected="selected"' : '').'>Yes</option>
	  <option value="0"'.((Configuration::get('SOCIALSTREAM_c10') == '0') ? 'selected="selected"' : '').'>No</option>
    </select>

		
		</div>
				<label>'.$this->l('Linkedin url').'</label>
				<div class="margin-form">
					<input type="text" size="50" name="c10t" value="'.Tools::getValue('c10t', Configuration::get('SOCIALSTREAM_c10t')).'" />
					<p class="clear">'.$this->l('Full url of your Linkedin page (leave blank to use share instead of a link to the site)').'</p>
					
				</div>-->
				
								
				<label>'.$this->l('Google+').'</label>
				<div class="margin-form">
					<input type="text" size="50" name="c12" value="'.Tools::getValue('c12', Configuration::get('SOCIALSTREAM_c12')).'" />
					<p class="clear">'.$this->l('like \'@+username\'. Use @ for user and # for tags, you can use various users or tags:\'@+username\',\'@+username2\'\'#tag1\'').'</p>
					
				</div>
				<label>'.$this->l('Post to show').'</label>
				<div class="margin-form">
					<input type="text" size="50" name="li4" value="'.Tools::getValue('li4', Configuration::get('SOCIALSTREAM_li4')).'" />
					<p class="clear">'.$this->l('Total of post to show').'</p>
					
				</div>
					<label>'.$this->l('Google Access token').'</label>
				<div class="margin-form">
					<input type="text" size="50" name="c121" value="'.Tools::getValue('c121', Configuration::get('SOCIALSTREAM_c121')).'" />
					<p class="clear">'.$this->l('Get google token from: https://console.developers.google.com').'</p>
					
				</div>
					
		<label>'.$this->l('Instagram user ID').'</label>
				<div class="margin-form">
					<input type="text" size="50" name="c13" value="'.Tools::getValue('c13', Configuration::get('SOCIALSTREAM_c13')).'" />
					<p class="clear">'.$this->l('like \'&userid\'. Use & for user and # for tags. Get your Instagram access token, and paste here to get your USER ID https://api.instagram.com/v1/users/self/?access_token=xxxx, you can use various users or tags:\'&username\',\'&username2\'\'#tag1\'').'</p>
					
				</div>
					<label>'.$this->l('Post to show').'</label>
				<div class="margin-form">
					<input type="text" size="50" name="li5" value="'.Tools::getValue('li5', Configuration::get('SOCIALSTREAM_li5')).'" />
					<p class="clear">'.$this->l('Total of post to show').'</p>
					
				</div>
					<label>'.$this->l('Instagram client ID').'</label>
				<div class="margin-form">
					<input type="text" size="50" name="c13t" value="'.Tools::getValue('c13t', Configuration::get('SOCIALSTREAM_c13t')).'" />
					<p class="clear">'.$this->l('Get the client ID here: https://instagram.com/developer/clients/manage/. After that, edit the client and in the security tab uncheck Disable implicit OAuth').'</p>
					
				</div>
								<label>'.$this->l('Instagram access token').'</label>
				<div class="margin-form">
					<input type="text" size="50" name="c122" value="'.Tools::getValue('c122', Configuration::get('SOCIALSTREAM_c122')).'" />
					<p class="clear">'.$this->l('Get the access token here: http://instagram.pixelunion.net/').'</p>
					
				</div>
				
				
						<label>'.$this->l('Pinterest user').'</label>
				<div class="margin-form">
					<input type="text" size="50" name="c13" value="'.Tools::getValue('c13', Configuration::get('SOCIALSTREAM_c13')).'" />
					<p class="clear">'.$this->l('like \'@username/boardname\'. Use @ for user and # for tags').'</p>
					
				</div>
					<label>'.$this->l('Post to show').'</label>
				<div class="margin-form">
					<input type="text" size="50" name="li6" value="'.Tools::getValue('li6', Configuration::get('SOCIALSTREAM_li6')).'" />
					<p class="clear">'.$this->l('Total of post to show').'</p>
					
				</div>

								<label>'.$this->l('Pinterest access token').'</label>
				<div class="margin-form">
					<input type="text" size="50" name="c123" value="'.Tools::getValue('c123', Configuration::get('SOCIALSTREAM_c123')).'" />
					<p class="clear">'.$this->l('Get the access token here: https://developers.pinterest.com/tools/access_token/').'</p>
					
				</div>
				
				
				
		<label>'.$this->l('Show Media').'</label>
	<div class="margin-form">
  <select name="c11" >
  <option value="1"'.((Configuration::get('SOCIALSTREAM_c11') == '1') ? 'selected="selected"' : '').'>Yes</option>
	  <option value="0"'.((Configuration::get('SOCIALSTREAM_c11') == '0') ? 'selected="selected"' : '').'>No</option>
    </select>

		
		</div>
			<label>'.$this->l('Shadow').'</label>
				<div class="margin-form">
					<input type="text" size="50" name="shadow" value="'.Tools::getValue('shadow', Configuration::get('SOCIALSTREAM_SHADOW')).'" />
					<p class="clear">'.$this->l('Shadow of elements like: 0 0 1px 0 rgba(10, 10, 10, 0.2)').'</p>
					
				</div>
			<label>'.$this->l('Bad words').'</label>
				<div class="margin-form">
					<input type="text" size="50" name="bad" value="'.Tools::getValue('bad', Configuration::get('SOCIALSTREAM_BAD')).'" />
					<p class="clear">'.$this->l('Hide posts wit hthis words (like: word1, word2, word3)').'</p>
					
				</div>
		<center> <p>'.$this->l('You can transplant the module into columns or footer. Go to modules, transplant a module, and transplant it, but the module can be only in one hook at time.').'</p></center><br/>
 <center> <p>'.$this->l('The module have a own page to show the steam, You can access with the url http://yourite/modules/prestasocialstream/prestasocialstream-page.php.').'</p></center><br/>
 
  <center> <p>'.$this->l('You can create box shadow codes in this URL:').' http://css3gen.com/box-shadow/</p></center><br/>

		
				<center><input type="submit" name="submitCoolshare" value="'.$this->l('Save').'" class="button" /></center><br/>
						<center><a href="../modules/prestasocialstream/moduleinstall.pdf"><img src="../modules/responsiveproducts/views/img/readme.png" style="  width: 32px;margin: 5px;  vertical-align: middle;" />README</a> / 
						<a href="../modules/prestasocialstream/termsandconditions.pdf"><img src="../modules/responsiveproducts/views/img/terms.png" style="  width: 32px;margin: 5px;  vertical-align: middle;" />TERMS</a></center><br/>
						
					<center>Video:<br/>
					<iframe width="640" height="360" src="https://www.youtube.com/embed/2fouybNI6kY?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
					</center>
			</fieldset>
		</form>
		';
        return $output;
    }

    /**
     * Returns module content
     *
     * @param array $params Parameters
     *
     * @return string Content
     */
    public function hookHome($params)
    {
        $widthcs = Configuration::get('SOCIALSTREAM_WIDTHCS');
        $width = Configuration::get('SOCIALSTREAM_WIDTH');
        $margin = Configuration::get('SOCIALSTREAM_MARGIN');
        $height = Configuration::get('SOCIALSTREAM_HEIGHT');
        $formatcs = Configuration::get('SOCIALSTREAM_FORMATCS');
        $float = Configuration::get('SOCIALSTREAM_FLOAT');
        $li1 = Configuration::get('SOCIALSTREAM_li1');
        $li2 = Configuration::get('SOCIALSTREAM_li2');
        $li3 = Configuration::get('SOCIALSTREAM_li3');
        $li4 = Configuration::get('SOCIALSTREAM_li4');
        $li5 = Configuration::get('SOCIALSTREAM_li5');
        $c1 = Configuration::get('SOCIALSTREAM_c1');
        $c2 = Configuration::get('SOCIALSTREAM_c2');
        $c3 = Configuration::get('SOCIALSTREAM_c3');
        $c4 = Configuration::get('SOCIALSTREAM_c4');
        $c5 = Configuration::get('SOCIALSTREAM_c5');
        $c6 = Configuration::get('SOCIALSTREAM_c6');
        $c7 = Configuration::get('SOCIALSTREAM_c7');
        $c8 = Configuration::get('SOCIALSTREAM_c8');
        $c9 = Configuration::get('SOCIALSTREAM_c9');
        $c10 = Configuration::get('SOCIALSTREAM_c10');
        $c11 = Configuration::get('SOCIALSTREAM_c11');
        $c12 = Configuration::get('SOCIALSTREAM_c12');
        $c13 = Configuration::get('SOCIALSTREAM_c13');
        $c1t = Configuration::get('SOCIALSTREAM_c1t');
        $c2t = Configuration::get('SOCIALSTREAM_c2t');
        $c3t = Configuration::get('SOCIALSTREAM_c3t');
        $c4t = Configuration::get('SOCIALSTREAM_c4t');
        $c5t = Configuration::get('SOCIALSTREAM_c5t');
        $c6t = Configuration::get('SOCIALSTREAM_c6t');
        $c7t = Configuration::get('SOCIALSTREAM_c7t');
        $c8t = Configuration::get('SOCIALSTREAM_c8t');
        $c9t = Configuration::get('SOCIALSTREAM_c9t');
        $c10t = Configuration::get('SOCIALSTREAM_c10t');
        $c11t = Configuration::get('SOCIALSTREAM_c11t');
        $c12t = Configuration::get('SOCIALSTREAM_c12t');
        $c13t = Configuration::get('SOCIALSTREAM_c13t');
        $colorize = Configuration::get('SOCIALSTREAM_COLORIZE');
        $servername = $_SERVER['SERVER_NAME'];
        $requesturi = $_SERVER['REQUEST_URI'];
        $url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
        $filea = dirname($url);
        $this->context->smarty->assign(
            array(
                'widthcs' => $widthcs,
                'psversion' => _PS_VERSION_,
                'width' => $width,
                'filea' => $filea,
                'margin' => $margin,
                'height' => (_PS_VERSION_ < '1.6.0.0' ? '#'.$height : $height),
                'formatsoc' => $formatcs,
                'float' => $float,
                'servername' => $servername,
                'requesturi' => $requesturi,
                'targetsoc' => $colorize,
                'c1soc' => $c1,
                'c2soc' => $c2,
                'c3soc' => $c3,
                'c4soc' => $c4,
                'c5soc' => $c5,
                'c6soc' => $c6,
                'c7soc' => $c7,
                'c8soc' => $c8,
                'c9soc' => $c9,
                'c10soc' => $c10,
                'c11soc' => $c11,
                'c12soc' => $c12,
                'c13soc' => $c13,
                'c1t' => $c1t,
                'c2t' => $c2t,
                'c3t' => $c3t,
                'c4t' => $c4t,
                'c5t' => $c5t,
                'c6t' => $c6t,
                'c7t' => $c7t,
                'c8t' => $c8t,
                'c9t' => $c9t,
                'c10t' => $c10t,
                'c11t' => $c11t,
                'c12t' => $c12t,
                'c13t' => $c13t,
            )
        );

        return $this->display(
            __FILE__,
            'views/templates/front/soc.tpl'
        );
    }

    public function hookLeftColumn($params)
    {
        $widthcs = Configuration::get('SOCIALSTREAM_WIDTHCS');
        $width = Configuration::get('SOCIALSTREAM_WIDTH');
        $margin = Configuration::get('SOCIALSTREAM_MARGIN');
        $height = Configuration::get('SOCIALSTREAM_HEIGHT');
        $formatcs = Configuration::get('SOCIALSTREAM_FORMATCS');
        $float = Configuration::get('SOCIALSTREAM_FLOAT');
        $li1 = Configuration::get('SOCIALSTREAM_li1');
        $li2 = Configuration::get('SOCIALSTREAM_li2');
        $li3 = Configuration::get('SOCIALSTREAM_li3');
        $li4 = Configuration::get('SOCIALSTREAM_li4');
        $li5 = Configuration::get('SOCIALSTREAM_li5');
        $c1 = Configuration::get('SOCIALSTREAM_c1');
        $c2 = Configuration::get('SOCIALSTREAM_c2');
        $c3 = Configuration::get('SOCIALSTREAM_c3');
        $c4 = Configuration::get('SOCIALSTREAM_c4');
        $c5 = Configuration::get('SOCIALSTREAM_c5');
        $c6 = Configuration::get('SOCIALSTREAM_c6');
        $c7 = Configuration::get('SOCIALSTREAM_c7');
        $c8 = Configuration::get('SOCIALSTREAM_c8');
        $c9 = Configuration::get('SOCIALSTREAM_c9');
        $c10 = Configuration::get('SOCIALSTREAM_c10');
        $c11 = Configuration::get('SOCIALSTREAM_c11');
        $c12 = Configuration::get('SOCIALSTREAM_c12');
        $c13 = Configuration::get('SOCIALSTREAM_c13');
        $c1t = Configuration::get('SOCIALSTREAM_c1t');
        $c2t = Configuration::get('SOCIALSTREAM_c2t');
        $c3t = Configuration::get('SOCIALSTREAM_c3t');
        $c4t = Configuration::get('SOCIALSTREAM_c4t');
        $c5t = Configuration::get('SOCIALSTREAM_c5t');
        $c6t = Configuration::get('SOCIALSTREAM_c6t');
        $c7t = Configuration::get('SOCIALSTREAM_c7t');
        $c8t = Configuration::get('SOCIALSTREAM_c8t');
        $c9t = Configuration::get('SOCIALSTREAM_c9t');
        $c10t = Configuration::get('SOCIALSTREAM_c10t');
        $c11t = Configuration::get('SOCIALSTREAM_c11t');
        $c12t = Configuration::get('SOCIALSTREAM_c12t');
        $c13t = Configuration::get('SOCIALSTREAM_c13t');
        $colorize = Configuration::get('SOCIALSTREAM_COLORIZE');
        $servername = $_SERVER['SERVER_NAME'];
        $requesturi = $_SERVER['REQUEST_URI'];
        $url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
        $filea = dirname($url);
        $this->context->smarty->assign(
            array(
                'widthcs' => $widthcs,
                'psversion' => _PS_VERSION_,
                'width' => $width,
                'filea' => $filea,
                'margin' => $margin,
                'height' => (_PS_VERSION_ < '1.6.0.0' ? '#'.$height : $height),
                'formatsoc' => $formatcs,
                'float' => $float,
                'servername' => $servername,
                'requesturi' => $requesturi,
                'targetsoc' => $colorize,
                'c1soc' => $c1,
                'c2soc' => $c2,
                'c3soc' => $c3,
                'c4soc' => $c4,
                'c5soc' => $c5,
                'c6soc' => $c6,
                'c7soc' => $c7,
                'c8soc' => $c8,
                'c9soc' => $c9,
                'c10soc' => $c10,
                'c11soc' => $c11,
                'c12soc' => $c12,
                'c13soc' => $c13,
                'c1t' => $c1t,
                'c2t' => $c2t,
                'c3t' => $c3t,
                'c4t' => $c4t,
                'c5t' => $c5t,
                'c6t' => $c6t,
                'c7t' => $c7t,
                'c8t' => $c8t,
                'c9t' => $c9t,
                'c10t' => $c10t,
                'c11t' => $c11t,
                'c12t' => $c12t,
                'c13t' => $c13t,
            )
        );

        return $this->display(
            __FILE__,
            'views/templates/front/soc3.tpl'
        );
    }

    private function _displayInfo()
    {
        return $this->display(
            __FILE__,
            'views/templates/hook/infos.tpl'
        );
    }

    private function _displayAdds()
    {
        return $this->display(
            __FILE__,
            'views/templates/hook/adds.tpl'
        );
    }

    public function hookRightColumn($params)
    {
        return $this->hookLeftColumn($params);
    }


    public function hookFooter($params)
    {
        return $this->hookHome($params);
    }

    public function hookProductFooter($params)
    {
        return $this->hookHome($params);
    }

    public function hookExtraLeft($params)
    {
        return $this->hookHome($params);
    }

    public function hookExtraRight($params)
    {
        return $this->hookHome($params);
    }

    public function hookHeader($params)
    {
        $pos1 = '';
        $pos2 = '';
        $widthcs = Configuration::get('SOCIALSTREAM_WIDTHCS');
        $bgcolorsoc = Configuration::get('SOCIALSTREAM_HEIGHT');
        $width = Configuration::get('SOCIALSTREAM_WIDTH');
        $margin = Configuration::get('SOCIALSTREAM_MARGIN');
        $formatcs = Configuration::get('SOCIALSTREAM_FORMATCS');
        $float = Configuration::get('SOCIALSTREAM_FLOAT');
        $c1 = Configuration::get('SOCIALSTREAM_c1');
        $c2 = Configuration::get('SOCIALSTREAM_c2');
        $c3 = Configuration::get('SOCIALSTREAM_c3');
        $c4 = Configuration::get('SOCIALSTREAM_c4');
        $c5 = Configuration::get('SOCIALSTREAM_c5');
        $c51 = Configuration::get('SOCIALSTREAM_c51');
        $c6 = Configuration::get('SOCIALSTREAM_c6');
        $c7 = Configuration::get('SOCIALSTREAM_c7');
        $c8 = Configuration::get('SOCIALSTREAM_c8');
        $c9 = Configuration::get('SOCIALSTREAM_c9');
        $bad = Configuration::get('SOCIALSTREAM_BAD');
        $li1 = Configuration::get('SOCIALSTREAM_li1');
        $li2 = Configuration::get('SOCIALSTREAM_li2');
        $li3 = Configuration::get('SOCIALSTREAM_li3');
        $li4 = Configuration::get('SOCIALSTREAM_li4');
        $li5 = Configuration::get('SOCIALSTREAM_li5');
        $li6 = Configuration::get('SOCIALSTREAM_li6');
        $c10 = Configuration::get('SOCIALSTREAM_c10');
        $c11 = Configuration::get('SOCIALSTREAM_c11');
        $c12 = Configuration::get('SOCIALSTREAM_c12');
        $c121 = Configuration::get('SOCIALSTREAM_c121');
        $c122 = Configuration::get('SOCIALSTREAM_c122');
        $c123 = Configuration::get('SOCIALSTREAM_c123');
        $c13 = Configuration::get('SOCIALSTREAM_c13');
        $c1t = Configuration::get('SOCIALSTREAM_c1t');
        $c2t = Configuration::get('SOCIALSTREAM_c2t');
        $c3t = Configuration::get('SOCIALSTREAM_c3t');
        $c4t = Configuration::get('SOCIALSTREAM_c4t');
        $c5t = Configuration::get('SOCIALSTREAM_c5t');
        $c6t = Configuration::get('SOCIALSTREAM_c6t');
        $c7t = Configuration::get('SOCIALSTREAM_c7t');
        $c8t = Configuration::get('SOCIALSTREAM_c8t');
        $c9t = Configuration::get('SOCIALSTREAM_c9t');
        $c10t = Configuration::get('SOCIALSTREAM_c10t');
        $c11t = Configuration::get('SOCIALSTREAM_c11t');
        $c12t = Configuration::get('SOCIALSTREAM_c12t');
        $c13t = Configuration::get('SOCIALSTREAM_c13t');
        $c14 = Configuration::get('SOCIALSTREAM_c14');
        $colorize = Configuration::get('SOCIALSTREAM_COLORIZE');
        $speeda = Configuration::get('SOCIALSTREAM_WIDTH');
        $float = Configuration::get('SOCIALSTREAM_FLOAT');
        $margin = Configuration::get('SOCIALSTREAM_MARGIN');
        $bgcolorsoc = Configuration::get('SOCIALSTREAM_HEIGHT');
        if ($li1 == null) {
            $li1 = 0;
        }
        if ($li2 == null) {
            $li2 = 0;
        }
        if ($li3 == null) {
            $li3 = 0;
        }
        if ($li4 == null) {
            $li4 = 0;
        }
        if ($li5 == null) {
            $li5 = 0;
        }
        if ($li6 == null) {
            $li6 = 0;
        }
        $url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
        $servername = $_SERVER['SERVER_NAME'];
        $requesturi = $_SERVER['REQUEST_URI'];
        $filea = dirname($url);
        if ($float == 1) {
            $pos1 = 'top';
            $pos2 = 'left';
        }
        if ($float == 2) {
            $pos1 = 'top';
            $pos2 = 'right';
        }
        if ($float == 3) {
            $pos1 = 'bottom';
            $pos2 = 'right';
        }
        if ($float == 4) {
            $pos1 = 'bottom';
            $pos2 = 'left';
        }
        if ($c11 == 1) {
            $c11 = 'true';
        } else {
            $c11 = 'false';
        }

        $this->context->smarty->assign(
            array(
                'psversion' => _PS_VERSION_,
                'speeda' => $speeda,
                'marginsoc' => $margin,
                'bgcolorsoc' => $bgcolorsoc,
                'pos1' => $pos1,
                'pos2' => $pos2,
                'widthcs' => $widthcs,
                'psversion' => _PS_VERSION_,
                'width' => $width,
                'filea' => $filea,
                'margin' => $margin,
                'bgcolorsoc' => (_PS_VERSION_ < '1.6.0.0' ? '#'.$bgcolorsoc : $bgcolorsoc),
                'formatsoc' => $formatcs,
                'float' => $float,
                'servername' => $servername,
                'requesturi' => $requesturi,
                'targetsoc' => $colorize,
                'c1' => $c1,
                'c2' => $c2,
                'c3' => $c3,
                'c4' => $c4,
                'c5' => $c5,
                'bad' => $bad,
                'li1' => $li1,
                'li2' => $li2,
                'li3' => $li3,
                'li4' => $li4,
                'li5' => $li5,
                'li6' => $li6,
                'c51' => $c51,
                'c6' => $c6,
                'c7' => $c7,
                'c8' => $c8,
                'c9' => $c9,
                'c10' => $c10,
                'c11' => $c11,
                'c12' => $c12,
                'c121' => $c121,
                'c122' => $c122,
                'c123' => $c123,
                'c13' => $c13,
                'c14' => $c14,
                'c1t' => $c1t,
                'c2t' => $c2t,
                'c3t' => $c3t,
                'c4t' => $c4t,
                'c5t' => $c5t,
                'c6t' => $c6t,
                'c7t' => $c7t,
                'c8t' => $c8t,
                'c9t' => $c9t,
                'c10t' => $c10t,
                'c11t' => $c11t,
                'c12t' => $c12t,
                'c13t' => $c13t,


            )
        );
        if (_PS_VERSION_ > '1.4.0.0' && _PS_VERSION_ < '1.5.0.0') {
            Tools::addCSS(
                __PS_BASE_URI__.'modules/prestasocialstream/views/css/font-awesome.min.css',
                'all'
            );
            Tools::addCSS(
                __PS_BASE_URI__.'modules/prestasocialstream/views/css/'.((_PS_VERSION_ > '1.5.0.0') ? $this->context->shop->id : '').'jquery.socialfeed.css',
                'all'
            );
            Tools::addJS(__PS_BASE_URI__.'modules/prestasocialstream/views/js/codebird.js');
            //Tools::addJS(__PS_BASE_URI__.'modules/prestasocialstream/views/js/masonry.js');
            Tools::addJS(__PS_BASE_URI__.'modules/prestasocialstream/views/js/doT.min.js');
            Tools::addJS(__PS_BASE_URI__.'modules/prestasocialstream/views/js/moment.js');
            Tools::addJS(__PS_BASE_URI__.'modules/prestasocialstream/views/js/jquery.socialfeed.js');
        }

        if (_PS_VERSION_ > '1.5.0.0') {
            $this->context->controller->addCSS(
                ($this->_path).'views/css/font-awesome.min.css',
                'all'
            );
            $this->context->controller->addCSS(
                ($this->_path).'views/css/'.((_PS_VERSION_ > '1.5.0.0') ? $this->context->shop->id : '').'jquery.socialfeed.css',
                'all'
            );
            //	$this->context->controller->addJS(($this->_path).'views/js/masonry.js');
            $this->context->controller->addJS(($this->_path).'views/js/codebird.js');
            $this->context->controller->addJS(($this->_path).'views/js/doT.min.js');
            $this->context->controller->addJS(($this->_path).'views/js/moment.js');
            $this->context->controller->addJS(($this->_path).'views/js/jquery.socialfeed.js');
        }

        if (_PS_VERSION_ < '1.7.0.0') {
            return $this->display(
                __FILE__,
                'views/templates/front/soc-header.tpl'
            );
        } else {
            return $this->display(
                __FILE__,
                'views/templates/front/soc-header-17.tpl'
            );
        }
    }
}
