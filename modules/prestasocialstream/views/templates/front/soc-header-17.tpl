{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if $psversion < "1.4.0.0"}
<link href="{$module_dir}css/main.css" rel="stylesheet" type="text/css">
{/if}
{literal}
<script>
(function(){"use strict";var c=[],f={},a,e,d,b;if(!window.jQuery){a=function(g){c.push(g)};f.ready=function(g){a(g)};e=window.jQuery=window.$=function(g){if(typeof g=="function"){a(g)}return f};window.checkJQ=function(){if(!d()){b=setTimeout(checkJQ,100)}};b=setTimeout(checkJQ,100);d=function(){if(window.jQuery!==e){clearTimeout(b);var g=c.shift();while(g){jQuery(g);g=c.shift()}b=f=a=e=d=window.checkJQ=null;return true}return false}}})();
</script>
{/literal}
{literal}
<script>
    $(document).ready(function(){
				

        $('.social-feed-container').socialfeed({
                    // FACEBOOK
					 plugin_folder:'{/literal}{$module_dir}{literal}',
					  template:'{/literal}{$module_dir}{literal}template.html',
                  {/literal}{if $c4t != ''}{literal}  facebook:{
                          accounts:[{/literal}{$c4t|replace:'&#039;':"'" nofilter}{literal}],
                        limit: {/literal}{$li1}{literal},
                        access_token: '{/literal}{$c4}{literal}' // APP_ID|APP_SECRET
                    },{/literal}{/if}{literal}
                    // TWITTER
                   {/literal}{if $c5t != ''}{literal}   twitter:{
               accounts: [{/literal}{$c5t|replace:'&#039;':"'" nofilter}{literal}],
                        limit: {/literal}{$li2}{literal},
                        consumer_key: '{/literal}{$c5}{literal}', // make sure to have your app read-only
                        consumer_secret: '{/literal}{$c51}{literal}', // make sure to have your app read-only
                     },{/literal}{/if}{literal}
					
                    // VK
                {/literal}{if $c10 != ''}{literal}      vk:{
                              accounts: [{/literal}{$c10|replace:'&#039;':"'" nofilter}{literal}], 
                        limit: {/literal}{$li3}{literal},
                        source: 'all'
                    },{/literal}{/if}{literal}
                    // GOOGLEPLUS
                {/literal}{if $c12 != ''}{literal}      google:{
                       accounts: [{/literal}{$c12|replace:'&#039;':"'" nofilter}{literal}],
                         limit:{/literal}{$li4}{literal},
                         access_token: '{/literal}{$c121}{literal}'
                     },{/literal}{/if}{literal}
                    // INSTAGRAM
                {/literal}{if $c13 != ''}{literal}         instagram:{
                       accounts: [{/literal}{$c13|replace:'&#039;':"'"|replace:'&amp;':'&' nofilter}{literal}],
                        limit:{/literal}{$li5}{literal},
                        client_id: '{/literal}{$c13t}{literal}',
						   access_token: '{/literal}{$c122}{literal}'
                    },{/literal}{/if}{literal}
					 //pinterest
					  {/literal}{if $c14 != ''}{literal}         pinterest:{
                       accounts: [{/literal}{$c14|replace:'&#039;':"'" nofilter}{literal}],
                        limit:{/literal}{$li6}{literal},
                        //client_id: '{/literal}{$c13t}{literal}',
                        access_token: '{/literal}{$c123}{literal}'

                    },{/literal}{/if}{literal} 
                  //  blogspot:{
                  //      accounts:['@greenworldmultilevel']
                  //  },
                    // GENERAL SETTINGS
                    length:{/literal}{$widthcs}{literal},
                    show_media:{/literal}{$c11}{literal},
                    // Moderation function - if returns false, template will have class hidden
                    moderation: function(content){
                        return  (content.text) ? content.text.indexOf('{/literal}{$bad}{literal}') == -1 : true;
                    },
                    //update_period: 5000,
                    // When all the posts are collected and displayed - this function is evoked
                    callback: function(){
                        console.log('all posts are collected');
                    }
                });
				$(".social-feed-text a:contains('read more')").html("{/literal}{l s='Leer' mod='prestasocialstream'|strip_tags:'UTF-8'}{literal}");

				moment().format('L');
        });
</script>
{/literal}