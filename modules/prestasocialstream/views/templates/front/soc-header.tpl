{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if $psversion < "1.4.0.0"}
<link href="{$module_dir|escape:'htmlall':'UTF-8'}css/main.css" rel="stylesheet" type="text/css">
{/if}

{literal}
<script>
    $(document).ready(function(){
        $('.social-feed-container').socialfeed({
                    // FACEBOOK
					 plugin_folder:'{/literal}{$module_dir|escape:'htmlall':'UTF-8'}{literal}',
					  template:'{/literal}{$module_dir|escape:'htmlall':'UTF-8'}{literal}template.html',
                  {/literal}{if $c4t != ''}{literal}  facebook:{
                          accounts:[{/literal}{$c4t|escape:'htmlall':'UTF-8'|replace:'&#039;':"'"}{literal}],
                        limit: {/literal}{$li1|escape:'htmlall':'UTF-8'}{literal},
                        access_token: '{/literal}{$c4|escape:'htmlall':'UTF-8'}{literal}' // APP_ID|APP_SECRET
                    },{/literal}{/if}{literal}
                    // TWITTER
                   {/literal}{if $c5t != ''}{literal}   twitter:{
               accounts: [{/literal}{$c5t|escape:'htmlall':'UTF-8'|replace:'&#039;':"'"}{literal}],
                        limit: {/literal}{$li2|escape:'htmlall':'UTF-8'}{literal},
                        consumer_key: '{/literal}{$c5|escape:'htmlall':'UTF-8'}{literal}', // make sure to have your app read-only
                        consumer_secret: '{/literal}{$c51|escape:'htmlall':'UTF-8'}{literal}', // make sure to have your app read-only
                     },{/literal}{/if}{literal}
					
                    // VK
                {/literal}{if $c10 != ''}{literal}      vk:{
                              accounts: [{/literal}{$c10|escape:'htmlall':'UTF-8'|replace:'&#039;':"'"}{literal}], 
                        limit: {/literal}{$li3|escape:'htmlall':'UTF-8'}{literal},
                        source: 'all'
                    },{/literal}{/if}{literal}
                    // GOOGLEPLUS
                {/literal}{if $c12 != ''}{literal}      google:{
                       accounts: [{/literal}{$c12|escape:'htmlall':'UTF-8'|replace:'&#039;':"'"}{literal}],
                         limit:{/literal}{$li4|escape:'htmlall':'UTF-8'}{literal},
                         access_token: '{/literal}{$c121|escape:'htmlall':'UTF-8'}{literal}'
                     },{/literal}{/if}{literal}
                    // INSTAGRAM
                {/literal}{if $c13 != ''}{literal}         instagram:{
                       accounts: [{/literal}{$c13|escape:'htmlall':'UTF-8'|replace:'&#039;':"'"|replace:'&amp;':'&'}{literal}],
                        limit:{/literal}{$li5|escape:'htmlall':'UTF-8'}{literal},
                        client_id: '{/literal}{$c13t|escape:'htmlall':'UTF-8'}{literal}',
						   access_token: '{/literal}{$c122|escape:'htmlall':'UTF-8'}{literal}'
                    },{/literal}{/if}{literal}
					 //pinterest
					  {/literal}{if $c14 != ''}{literal}         pinterest:{
                       accounts: [{/literal}{$c14|escape:'htmlall':'UTF-8'|replace:'&#039;':"'"}{literal}],
                        limit:{/literal}{$li6|escape:'htmlall':'UTF-8'}{literal},
                        //client_id: '{/literal}{$c13t|escape:'htmlall':'UTF-8'}{literal}',
                        access_token: '{/literal}{$c123|escape:'htmlall':'UTF-8'}{literal}'

                    },{/literal}{/if}{literal} 
                  //  blogspot:{
                  //      accounts:['@greenworldmultilevel']
                  //  },
                    // GENERAL SETTINGS
                    length:{/literal}{$widthcs|escape:'htmlall':'UTF-8'}{literal},
                    show_media:{/literal}{$c11|escape:'htmlall':'UTF-8'}{literal},
                    // Moderation function - if returns false, template will have class hidden
                    moderation: function(content){
                        return  (content.text) ? content.text.indexOf('{/literal}{$bad|escape:"htmlall":"UTF-8"}{literal}') == -1 : true;
                    },
                    //update_period: 5000,
                    // When all the posts are collected and displayed - this function is evoked
                    callback: function(){
                        console.log('all posts are collected');
                    }
                });
				$(".social-feed-text a:contains('read more')").html("{/literal}{l s='Leer' mod='prestasocialstream'|strip_tags:'UTF-8'}{literal}");

				moment().format('L');
        });
		
</script>
{/literal}