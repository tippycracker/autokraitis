{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}


<div class="alert alert-message"> 
 <p>{l s='You can transplant the module into columns or footer. Go to modules, transplant a module, and transplant it, but the module can be only in one hook at time.' mod='prestasocialstream'}</p>
  <p>{l s='The module have a own page to show the steam, You can access with the url http://yourite/modules/prestasocialstream/prestasocialstream-page.php.' mod='prestasocialstream'}</p>
  <p>{l s='You can create box shadow codes in this URL:' mod='prestasocialstream'}<a href="http://css3gen.com/box-shadow/" target="_blank"> http://css3gen.com/box-shadow/</a> </p>

	<center><a href="{$module_dir|escape:'htmlall':'UTF-8'}moduleinstall.pdf" target="_blank"><img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/readme.png" style="  width: 31px;margin: 5px;" />README</a> / 
		<a href="{$module_dir|escape:'htmlall':'UTF-8'}termsandconditions.pdf" target="_blank"><img src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/terms.png" style="  width: 31px;margin: 5px;" />TERMS</a></center>
</div>
