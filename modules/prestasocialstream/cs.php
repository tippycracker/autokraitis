<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{socialtech}prestashop>socialtech_864eddff9ce5adb6699cbd2cff995ae3'] = 'Blok plovoucí sociální sdílení';
$_MODULE['<{socialtech}prestashop>socialtech_e587cbc71aee2f6cad2a335a4009c22e'] = 'Přidá plovoucí blok zobrazení záložek';
$_MODULE['<{socialtech}prestashop>socialtech_c888438d14855d7d96a2724ee9c306bd'] = 'Nastavení aktualizace';
$_MODULE['<{socialtech}prestashop>socialtech_f4f70727dc34561dfde1a3c529b6205c'] = 'Nastavení';
$_MODULE['<{socialtech}prestashop>socialtech_e924201b842388eae4bd0f8f1d2b7107'] = 'Šířka obrázků';
$_MODULE['<{socialtech}prestashop>socialtech_36244c9637c0b6dcd6a0061cf90f785b'] = 'Šířka obrázků';
$_MODULE['<{socialtech}prestashop>socialtech_3a6d2d67d8d3e7c3dbcc3b3982f12a0a'] = 'Rychlost animace';
$_MODULE['<{socialtech}prestashop>socialtech_0e058d928350704a0572507bd03c76b4'] = 'Rychlost animace (výchozí 200)';
$_MODULE['<{socialtech}prestashop>socialtech_368d9ac76af05f714092bc808a426bfc'] = 'Barva pozadí';
$_MODULE['<{socialtech}prestashop>socialtech_cafe1b94f70c18b134b6e2c57746fbce'] = 'Barva karty (výchozí ffffff)';
$_MODULE['<{socialtech}prestashop>socialtech_75794fe8ae8ecc3c2a6210e3ec3824cf'] = 'Od horního nebo dolního okraje';
$_MODULE['<{socialtech}prestashop>socialtech_c41a31890959544c6523af684561abe5'] = 'Cíl';
$_MODULE['<{socialtech}prestashop>socialtech_adb2e92a7c1c15f55fe83554083fc246'] = 'Otevření odkazů v novém okně';
$_MODULE['<{socialtech}prestashop>socialtech_d0efb46cfd8b126482192a343e2142b4'] = 'Formát obrázku';
$_MODULE['<{socialtech}prestashop>socialtech_52f5e0bc3859bc5f5e25130b6c7e8881'] = 'Pozice';
$_MODULE['<{socialtech}prestashop>socialtech_3832fc9ce55fc92562c0e4b6aac54def'] = 'Technorati';
$_MODULE['<{socialtech}prestashop>socialtech_e4c61e63437c8c315ba973a2fa1bb5f0'] = 'Technorati url';
$_MODULE['<{socialtech}prestashop>socialtech_7d2c1dd0fe22673c11ccd481f46af07a'] = 'Úplná adresa url stránky Technorati (nechte prázdné místo odkazu na web použít podíl)';
$_MODULE['<{socialtech}prestashop>socialtech_898153e542ab70aa558a97a80955830d'] = 'Lahodné';
$_MODULE['<{socialtech}prestashop>socialtech_f8e48612cec7fa70a41453379fdcc38a'] = 'Vynikající url';
$_MODULE['<{socialtech}prestashop>socialtech_fb6644251aef5e594752733dda0165d1'] = 'Úplná adresa url výborné stránky (nechte prázdné místo odkazu na web použít podíl)';
$_MODULE['<{socialtech}prestashop>socialtech_b632c55a33530d1433e29ffc09ba1151'] = 'Reddit';
$_MODULE['<{socialtech}prestashop>socialtech_d381875976aaa9ddbccab5b05f399b49'] = 'Reddit url';
$_MODULE['<{socialtech}prestashop>socialtech_d2ac5aa86fc36a5f29777d8ab6a9f73e'] = 'Úplná adresa url stránky Červenavý (nechte prázdné místo odkazu na web použít podíl)';
$_MODULE['<{socialtech}prestashop>socialtech_d85544fce402c7a2a96a48078edaf203'] = 'Facebook';
$_MODULE['<{socialtech}prestashop>socialtech_e6d8204cbe68167a3994adf88dd8f9f3'] = 'Facebook url';
$_MODULE['<{socialtech}prestashop>socialtech_cb4bb7ca95a71e0ba37c2becb1a963be'] = 'Úplná adresa url stránky Facebook (nechte prázdné místo odkazu na web použít podíl)';
$_MODULE['<{socialtech}prestashop>socialtech_2491bc9c7d8731e1ae33124093bc7026'] = 'Twitter';
$_MODULE['<{socialtech}prestashop>socialtech_309d0fd278a2d5ada43998d4eafcd3a4'] = 'Rozechvění url';
$_MODULE['<{socialtech}prestashop>socialtech_95d4bef3e984d8ec53b3b55d3f0e8aa2'] = 'Úplná adresa url stránky Twitter (nechte prázdné místo odkazu na web použít podíl)';
$_MODULE['<{socialtech}prestashop>socialtech_c46526afaf8301c71b765234ae23dc8b'] = 'StumbleUpon';
$_MODULE['<{socialtech}prestashop>socialtech_6122e6753b34f199ede5bfbcc05430c2'] = 'StumbleUpon url';
$_MODULE['<{socialtech}prestashop>socialtech_fc8483dbed69bf414c02b514ec7170d9'] = 'Úplná adresa url stránky Stumbleupon (nechte prázdné místo odkazu na web použít podíl)';
$_MODULE['<{socialtech}prestashop>socialtech_1334b6ec0ff0dc970481738a2374448c'] = 'Yahoo';
$_MODULE['<{socialtech}prestashop>socialtech_a4cd3bb8d3f54f42a3bf40bc31d568ab'] = 'Yahoo url';
$_MODULE['<{socialtech}prestashop>socialtech_6699ae5d84461afef2fbb70cbb315b17'] = 'Úplná adresa url stránky Yahoo (nechte prázdné místo odkazu na web použít podíl)';
$_MODULE['<{socialtech}prestashop>socialtech_9c393f1c3d00f6f6ab7f2f393508cf5d'] = 'Digg';
$_MODULE['<{socialtech}prestashop>socialtech_3b8fc56e4c58accc518452c103b3990e'] = 'Doupě url';
$_MODULE['<{socialtech}prestashop>socialtech_603f00bf9e2fa7a32e87683ac328291f'] = 'Úplná adresa url stránky Digg (nechte prázdné místo odkazu na web použít podíl)';
$_MODULE['<{socialtech}prestashop>socialtech_3b0f3a25d07e5d9dbdf98db15ee70410'] = 'Flickr';
$_MODULE['<{socialtech}prestashop>socialtech_44e2308cdc51a0a1a5350ba3937e0b5e'] = 'LinkedIn';
$_MODULE['<{socialtech}prestashop>socialtech_1d14791ad69312ad279b09c9ac205879'] = 'LinkedIn url';
$_MODULE['<{socialtech}prestashop>socialtech_41db961d0a03478919762ffb51bab6fb'] = 'Úplná adresa url stránky Linkedin (nechte prázdné místo odkazu na web použít podíl)';
$_MODULE['<{socialtech}prestashop>socialtech_5472f6a0741f01691de6f092c16dfc53'] = 'Google + 1';
$_MODULE['<{socialtech}prestashop>socialtech_291d943f5a3af57fd593373d786b63f0'] = 'Google + 1 url';
$_MODULE['<{socialtech}prestashop>socialtech_9ca0d95bd980836d4664b914969ec209'] = 'Úplná adresa url stránky Google + 1 (nechte prázdné místo odkazu na web použít podíl)';
$_MODULE['<{socialtech}prestashop>socialtech_86709a608bd914b28221164e6680ebf7'] = 'Pinterest';
$_MODULE['<{socialtech}prestashop>socialtech_06485b59fa696c49c39095ab26556140'] = 'Pinterest url';
$_MODULE['<{socialtech}prestashop>socialtech_fc911991a7be4fbff2621ecbdef902af'] = 'Úplná adresa url stránky Pinterest (nechte prázdné místo odkazu na web použít podíl)';
$_MODULE['<{socialtech}prestashop>socialtech_4dfbb099eafd3c82e033bf92946d3ce6'] = 'Pošta';
$_MODULE['<{socialtech}prestashop>socialtech_c9cc8cce247e49bae79f15173ce97354'] = 'Uložit';
