<?php
/**
 * 2007-2014 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * No redistribute in other sites, or copy.
 *
 * @author    RSI <demo@demo.com>
 * @copyright 2007-2016 RSI
 * @license   http://localhost
 */

include_once(dirname(__FILE__).'/../../config/config.inc.php');
if (_PS_VERSION_ < '1.5.0.0') {
    @include_once(dirname(__FILE__).'/../../header.php');
    require_once(dirname(__FILE__).'/prestasocialstream.php');
    $errors = array();
    $params = '';
    @$prestasocialstream = new PrestaSocialStream();
    echo $prestasocialstream->displayFrontForm($params);
    @include(dirname(__FILE__).'/../../footer.php');
} else {
    if (_PS_VERSION_ < '1.5.3.0') {
        @include_once(dirname(__FILE__).'/../../header.php');
    }
    require_once(dirname(__FILE__).'/prestasocialstream.php');
    if (_PS_VERSION_ > '1.5.0.0') {
        $context = Context::getContext();
    }
    $errors = array();
    $controller = new FrontController();
    $controller->init();
    @$prestasocialstream = new PrestaSocialStream();

    Tools::redirect(
        Context::getContext()->link->getModuleLink(
            'prestasocialstream',
            'default'
        )
    );
}
