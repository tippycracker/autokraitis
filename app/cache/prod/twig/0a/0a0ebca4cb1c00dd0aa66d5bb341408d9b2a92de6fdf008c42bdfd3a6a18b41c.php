<?php

/* __string_template__d6d5286ad6249b4aa0ed2d6b53adee7e964a2373d05eb1641c788140406f1bee */
class __TwigTemplate_bed1b0b80a633ba82400f7dfa5f9b7eda38136b777b5c437a1dfc60cb0968947 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'extra_stylesheets' => array($this, 'block_extra_stylesheets'),
            'content_header' => array($this, 'block_content_header'),
            'content' => array($this, 'block_content'),
            'content_footer' => array($this, 'block_content_footer'),
            'sidebar_right' => array($this, 'block_sidebar_right'),
            'javascripts' => array($this, 'block_javascripts'),
            'extra_javascripts' => array($this, 'block_extra_javascripts'),
            'translate_javascripts' => array($this, 'block_translate_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"lt\">
<head>
  <meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=0.75, maximum-scale=0.75, user-scalable=0\">
<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">
<meta name=\"robots\" content=\"NOFOLLOW, NOINDEX\">

<link rel=\"icon\" type=\"image/x-icon\" href=\"/img/favicon.ico\" />
<link rel=\"apple-touch-icon\" href=\"/img/app_icon.png\" />

<title>Modulio pasirinkimas • Autokraitis</title>

  <script type=\"text/javascript\">
    var help_class_name = 'AdminModules';
    var iso_user = 'lt';
    var lang_is_rtl = '0';
    var full_language_code = 'lt-lt';
    var full_cldr_language_code = 'lt-LT';
    var country_iso_code = 'LT';
    var _PS_VERSION_ = '1.7.3.1';
    var roundMode = 2;
    var youEditFieldFor = '';
        var new_order_msg = 'Parduotuvėje atliktas naujas užsakymas.';
    var order_number_msg = 'Užsakymo numeris: ';
    var total_msg = 'Viso: ';
    var from_msg = 'Nuo: ';
    var see_order_msg = 'Peržiūrėti šį užsakymą';
    var new_customer_msg = 'Parduotuvėje užsiregistravo naujas pirkėjas.';
    var customer_name_msg = 'Kliento vardas: ';
    var new_msg = 'Gauta nauja žinutė jūsų parduotuvėje.';
    var see_msg = 'Skaityti šią žinutę';
    var token = 'b873b69bc9abcd73086d834420995c82';
    var token_admin_orders = '531e75f65321d6cfb93ae2128e8c29b8';
    var token_admin_customers = 'eb3a0027b3099246fd7e4202bbe51ffa';
    var token_admin_customer_threads = 'fbddd3dd1b04b3ff67c5b5e96e3102af';
    var currentIndex = 'index.php?controller=AdminModules';
    var employee_token = '1a8d9ab18d7db7f78b62289b4862e360';
    var choose_language_translate = 'Pasirinkite kalbą';
    var default_language = '2';
    var admin_modules_link = '/admin786i0moky/index.php/module/catalog/recommended?route=admin_module_catalog_post&_token=NeiSOjlZRR3qgjA2POIi9wIK6IBwZ64oANabLb6N1ig';
    var tab_modules_list = '';
    var update_success_msg = 'Atnaujinta';
    var errorLogin = 'PrestaShop nepavyko prisijungti prie Addons. Patikrinkite savo prisijungimo duomenis ir interneto ryšį.';
    var search_product_msg = 'Ieškoti prekės';
  </script>

      <link href=\"/modules/gamification/views/css/gamification.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/admin786i0moky/themes/new-theme/public/theme.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/js/jquery/plugins/fancybox/jquery.fancybox.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/js/jquery/plugins/chosen/jquery.chosen.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/admin786i0moky/themes/default/css/vendor/nv.d3.css\" rel=\"stylesheet\" type=\"text/css\"/>
  
  <script type=\"text/javascript\">
var baseAdminDir = \"\\/admin786i0moky\\/\";
var baseDir = \"\\/\";
var currency = {\"iso_code\":\"EUR\",\"sign\":\"\\u20ac\",\"name\":\"Euras\",\"format\":\"#,##0.00\\u00a0\\u00a4\"};
var host_mode = false;
var show_new_customers = \"1\";
var show_new_messages = false;
var show_new_orders = \"1\";
</script>
<script type=\"text/javascript\" src=\"/js/jquery/jquery-1.11.0.min.js\"></script>
<script type=\"text/javascript\" src=\"/js/jquery/jquery-migrate-1.2.1.min.js\"></script>
<script type=\"text/javascript\" src=\"/modules/gamification/views/js/gamification_bt.js\"></script>
<script type=\"text/javascript\" src=\"/js/jquery/plugins/fancybox/jquery.fancybox.js\"></script>
<script type=\"text/javascript\" src=\"/admin786i0moky/themes/new-theme/public/main.bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/jquery/plugins/jquery.chosen.js\"></script>
<script type=\"text/javascript\" src=\"/js/admin.js?v=1.7.3.1\"></script>
<script type=\"text/javascript\" src=\"/js/cldr.js\"></script>
<script type=\"text/javascript\" src=\"/js/tools.js?v=1.7.3.1\"></script>
<script type=\"text/javascript\" src=\"/admin786i0moky/public/bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/vendor/d3.v3.min.js\"></script>
<script type=\"text/javascript\" src=\"/admin786i0moky/themes/default/js/vendor/nv.d3.min.js\"></script>


  <script>
\t\t\t\tvar ids_ps_advice = new Array();
\t\t\t\tvar admin_gamification_ajax_url = 'http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminGamification&token=c27dbf211d520b9a19cf9075b2b9eb42';
\t\t\t\tvar current_id_tab = 45;
\t\t\t</script>

";
        // line 83
        $this->displayBlock('stylesheets', $context, $blocks);
        $this->displayBlock('extra_stylesheets', $context, $blocks);
        echo "</head>
<body class=\"lang-lt adminmodules\">



<header id=\"header\">
  <nav id=\"header_infos\" class=\"main-header\">

    <button class=\"btn btn-primary-reverse onclick btn-lg unbind ajax-spinner\"></button>

    
    

    
    <i class=\"material-icons float-left px-1 js-mobile-menu d-md-none\">menu</i>
    <a id=\"header_logo\" class=\"logo float-left\" href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminDashboard&amp;token=22f4e1634756351f72549d53595eb1f4\"></a>

    <div class=\"component d-none d-md-flex\" id=\"quick-access-container\"><div class=\"dropdown quick-accesses\">
  <button class=\"btn btn-link btn-sm dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" id=\"quick_select\">
    Greita prieiga
  </button>
  <div class=\"dropdown-menu\">
          <a class=\"dropdown-item\"
         href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php/module/manage?token=a9e3d26c9d88536223df4c78f967e195\"
                 data-item=\"Įdiegti moduliai\"
      >Įdiegti moduliai</a>
          <a class=\"dropdown-item\"
         href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminStats&amp;module=statscheckup&amp;token=fc402d21533716abd2253b487f4b8347\"
                 data-item=\"Katalogo vertinimas\"
      >Katalogo vertinimas</a>
          <a class=\"dropdown-item\"
         href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminCategories&amp;addcategory&amp;token=b145228fca80a031f2ec1264aad32069\"
                 data-item=\"Nauja kategorija\"
      >Nauja kategorija</a>
          <a class=\"dropdown-item\"
         href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php/product/new?token=a9e3d26c9d88536223df4c78f967e195\"
                 data-item=\"Nauja prekė\"
      >Nauja prekė</a>
          <a class=\"dropdown-item\"
         href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminCartRules&amp;addcart_rule&amp;token=3aca16ebc64e4d0bb6d20f3a2b0d7519\"
                 data-item=\"Naujas kuponas\"
      >Naujas kuponas</a>
          <a class=\"dropdown-item\"
         href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminOrders&amp;token=531e75f65321d6cfb93ae2128e8c29b8\"
                 data-item=\"Užsakymai\"
      >Užsakymai</a>
          <a class=\"dropdown-item\"
         href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminModules&amp;&amp;configure=xipblog&amp;token=b873b69bc9abcd73086d834420995c82\"
                 data-item=\"XipBlog Settings\"
      >XipBlog Settings</a>
        <div class=\"dropdown-divider\"></div>
          <a
        class=\"dropdown-item js-quick-link\"
        href=\"#\"
        data-rand=\"55\"
        data-icon=\"icon-AdminParentModulesSf\"
        data-method=\"add\"
        data-url=\"index.php/module/catalog\"
        data-post-link=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminQuickAccesses&token=761b042ae047691daa35ebbee89a02d4\"
        data-prompt-text=\"Įveskite pavadinimą šiai santrumpai:\"
        data-link=\" - Sąra&scaron;as\"
      >
        <i class=\"material-icons\">add_circle</i>
        Pridėti puslapį į greitą prieigą
      </a>
        <a class=\"dropdown-item\" href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminQuickAccesses&token=761b042ae047691daa35ebbee89a02d4\">
      <i class=\"material-icons\">settings</i>
      Tvarkyti greitą prieigą
    </a>
  </div>
</div>
</div>
    <div class=\"component d-none d-md-inline-block col-md-4\" id=\"header-search-container\">
<form id=\"header_search\"
      class=\"bo_search_form dropdown-form js-dropdown-form collapsed\"
      method=\"post\"
      action=\"/admin786i0moky/index.php?controller=AdminSearch&amp;token=70b75cf59edcb8dccf6dce2b9ede75df\"
      role=\"search\">
  <input type=\"hidden\" name=\"bo_search_type\" id=\"bo_search_type\" class=\"js-search-type\" />
    <div class=\"input-group\">
    <input type=\"text\" class=\"form-control js-form-search\" id=\"bo_query\" name=\"bo_query\" value=\"\" placeholder=\"Paieška (pvz.: prekės kodas, kliento vardas...)\">
    <div class=\"input-group-btn\">
      <button type=\"button\" class=\"btn btn-outline-secondary dropdown-toggle js-dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
        Visur
      </button>
      <div class=\"dropdown-menu js-items-list\">
        <a class=\"dropdown-item\" data-item=\"Visur\" href=\"#\" data-value=\"0\" data-placeholder=\"Ko jūs ieškote?\" data-icon=\"icon-search\"><i class=\"material-icons\">search</i> Visur</a>
        <div class=\"dropdown-divider\"></div>
        <a class=\"dropdown-item\" data-item=\"Katalogas\" href=\"#\" data-value=\"1\" data-placeholder=\"Prekės pavadinimas, SKU, kodas...\" data-icon=\"icon-book\"><i class=\"material-icons\">store_mall_directory</i> Katalogas</a>
        <a class=\"dropdown-item\" data-item=\"Klientai pagal vardą\" href=\"#\" data-value=\"2\" data-placeholder=\"El. paštas, vardas...\" data-icon=\"icon-group\"><i class=\"material-icons\">group</i> Klientai pagal vardą</a>
        <a class=\"dropdown-item\" data-item=\"Klientai pagal IP adresą\" href=\"#\" data-value=\"6\" data-placeholder=\"123.45.67.80\" data-icon=\"icon-desktop\"><i class=\"material-icons\">desktop_mac</i> Klientai pagal IP adresą</a>
        <a class=\"dropdown-item\" data-item=\"Užsakymai\" href=\"#\" data-value=\"3\" data-placeholder=\"Užsakymo ID\" data-icon=\"icon-credit-card\"><i class=\"material-icons\">shopping_basket</i> Užsakymai</a>
        <a class=\"dropdown-item\" data-item=\"Sąskaitos\" href=\"#\" data-value=\"4\" data-placeholder=\"Sąskaitos numeris\" data-icon=\"icon-book\"><i class=\"material-icons\">book</i></i> Sąskaitos</a>
        <a class=\"dropdown-item\" data-item=\"Krepšeliai\" href=\"#\" data-value=\"5\" data-placeholder=\"Krepšelio ID\" data-icon=\"icon-shopping-cart\"><i class=\"material-icons\">shopping_cart</i> Krepšeliai</a>
        <a class=\"dropdown-item\" data-item=\"Moduliai\" href=\"#\" data-value=\"7\" data-placeholder=\"Modulio pavadinimas\" data-icon=\"icon-puzzle-piece\"><i class=\"material-icons\">extension</i> Moduliai</a>
      </div>
      <button class=\"btn btn-primary\" type=\"submit\"><span class=\"d-none\">PAIEŠKA</span><i class=\"material-icons\">search</i></button>
    </div>
  </div>
</form>

<script type=\"text/javascript\">
 \$(document).ready(function(){
    \$('#bo_query').one('click', function() {
    \$(this).closest('form').removeClass('collapsed');
  });
});
</script>
</div>

            <div class=\"component d-none d-md-inline-block\">  <div class=\"shop-list\">
    <a class=\"link\" id=\"header_shopname\" href=\"http://autokrai.kraitis.lt/\" target= \"_blank\">Autokraitis</a>
  </div>
</div>
          <div class=\"component\"><div id=\"notif\" class=\"notification-center dropdown dropdown-clickable\">
  <div class=\"notification js-notification dropdown-toggle\" data-toggle=\"dropdown\">
    <i class=\"material-icons\">notifications_none</i>
    <span id=\"notifications-total\" class=\"count hide\">0</span>
  </div>
  <div class=\"dropdown-menu dropdown-menu-right js-notifs_dropdown\">
    <div class=\"notifications\">
      <ul class=\"nav nav-tabs\" role=\"tablist\">
                          <li class=\"nav-item\">
            <a
              class=\"nav-link active\"
              id=\"orders-tab\"
              data-toggle=\"tab\"
              data-type=\"order\"
              href=\"#orders-notifications\"
              role=\"tab\"
            >
              Užsakymai<span id=\"_nb_new_orders_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"customers-tab\"
              data-toggle=\"tab\"
              data-type=\"customer\"
              href=\"#customers-notifications\"
              role=\"tab\"
            >
              Klientai<span id=\"_nb_new_customers_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"messages-tab\"
              data-toggle=\"tab\"
              data-type=\"customer_message\"
              href=\"#messages-notifications\"
              role=\"tab\"
            >
              Pranešimas<span id=\"_nb_new_messages_\"></span>
            </a>
          </li>
                        </ul>

      <!-- Tab panes -->
      <div class=\"tab-content\">
                          <div class=\"tab-pane active empty\" id=\"orders-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Šiuo metu naujų užsakymų nėra :(<br>
              Ar patikrinote savo <strong><a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminCarts&token=158343407ce5ea68670e358faca3cd01&action=filterOnlyAbandonedCarts\"> apleistą prekių krepšelį </a></strong>? <br>Jūsų kitas užsakymas gali būti slepiamas ten!
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"customers-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Šiuo metu naujų klientų nėra :(<br>
              Ar šiomis dienomis aktyviai dalyvaujate socialiniuose tinkluose?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"messages-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Šiuo metu naujų žinučių nėra.<br>
              Jokių žinių yra gerai, tiesa?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                        </div>
    </div>
  </div>
</div>

  <script type=\"text/html\" id=\"order-notification-template\">
    <a class=\"notif\" href='order_url'>
      #_id_order_ -
      nuo <strong>_customer_name_</strong> (_iso_code_)_carrier_
      <strong class=\"float-sm-right\">_total_paid_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"customer-notification-template\">
    <a class=\"notif\" href='customer_url'>
      #_id_customer_ - <strong>_customer_name_</strong>_company_ - registruotas <strong>_date_add_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"message-notification-template\">
    <a class=\"notif\" href='message_url'>
    <span class=\"message-notification-status _status_\">
      <i class=\"material-icons\">fiber_manual_record</i> _status_
    </span>
      - <strong>_customer_name_</strong> (_company_) - <i class=\"material-icons\">access_time</i> _date_add_
    </a>
  </script>
</div>
        <div class=\"component -norightmargin d-none d-md-inline-block\"><div class=\"employee-dropdown dropdown\">
      <div class=\"person\" data-toggle=\"dropdown\">
      <i class=\"material-icons\">account_circle</i>
    </div>
    <div class=\"dropdown-menu dropdown-menu-right\">
    <div class=\"text-xs-center employee_avatar\">
      <img class=\"avatar rounded-circle\" src=\"http://profile.prestashop.com/airidas.beno%40gmail.com.jpg\" /><br>
      <span>Airidas Benokraitis</span>
    </div>
    <div>
      <a class=\"employee-link profile-link\" href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminEmployees&amp;token=1a8d9ab18d7db7f78b62289b4862e360&amp;id_employee=1&amp;updateemployee\">
        <i class=\"material-icons\">settings_applications</i> Jūsų profilis
      </a>
    </div>
    <div>
      <a class=\"employee-link\" id=\"header_logout\" href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminLogin&amp;token=5aaa0781139af82b2cb5bb41cd815385&amp;logout\">
        <i class=\"material-icons\">power_settings_new</i> Atsijungti
      </a>
    </div>
  </div>
</div>
</div>

    
  </nav>
  </header>

<nav class=\"nav-bar\">
  <ul class=\"main-menu\">

          
                
                
        
          <li class=\"link-levelone \" data-submenu=\"1\" id=\"tab-AdminDashboard\">
            <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminDashboard&amp;token=22f4e1634756351f72549d53595eb1f4\" class=\"link\" >
              <i class=\"material-icons\">trending_up</i> <span>Skydelis</span>
            </a>
          </li>

        
                
                                  
                
        
          <li class=\"category-title d-none d-sm-block \" data-submenu=\"2\" id=\"tab-SELL\">
              <span class=\"title\">Pardavimai</span>
          </li>

                          
                
                                
                <li class=\"link-levelone \" data-submenu=\"3\" id=\"subtab-AdminParentOrders\">
                  <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminOrders&amp;token=531e75f65321d6cfb93ae2128e8c29b8\" class=\"link\">
                    <i class=\"material-icons\">shopping_basket</i>
                    <span>
                    Užsakymai
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-3\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"4\" id=\"subtab-AdminOrders\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminOrders&amp;token=531e75f65321d6cfb93ae2128e8c29b8\" class=\"link\"> Užsakymai
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"5\" id=\"subtab-AdminInvoices\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminInvoices&amp;token=a65d360aac7ff254638ba38780998a9c\" class=\"link\"> Sąskaitos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"6\" id=\"subtab-AdminSlip\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminSlip&amp;token=8b6c224a7997b491ef95ca73c027af11\" class=\"link\"> Grąžinimo kuponai
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"7\" id=\"subtab-AdminDeliverySlip\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminDeliverySlip&amp;token=5bfa5983e7ffea6323aabfa05d223bc1\" class=\"link\"> Pristatymo kvitai
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"8\" id=\"subtab-AdminCarts\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminCarts&amp;token=158343407ce5ea68670e358faca3cd01\" class=\"link\"> Prekių krepšeliai
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"9\" id=\"subtab-AdminCatalog\">
                  <a href=\"/admin786i0moky/index.php/product/catalog?_token=NeiSOjlZRR3qgjA2POIi9wIK6IBwZ64oANabLb6N1ig\" class=\"link\">
                    <i class=\"material-icons\">store</i>
                    <span>
                    Katalogas
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-9\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"10\" id=\"subtab-AdminProducts\">
                              <a href=\"/admin786i0moky/index.php/product/catalog?_token=NeiSOjlZRR3qgjA2POIi9wIK6IBwZ64oANabLb6N1ig\" class=\"link\"> Prekės
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"11\" id=\"subtab-AdminCategories\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminCategories&amp;token=b145228fca80a031f2ec1264aad32069\" class=\"link\"> Kategorijos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"12\" id=\"subtab-AdminTracking\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminTracking&amp;token=79bfe509748a0c6f0e455bb3a28d9ab8\" class=\"link\"> Kontrolė
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"13\" id=\"subtab-AdminParentAttributesGroups\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminAttributesGroups&amp;token=f1efc5a8370e22b9ef675acec032d938\" class=\"link\"> Savybės ir ypatybės
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"16\" id=\"subtab-AdminParentManufacturers\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminManufacturers&amp;token=c9221c4fcb6371c2e08d13009575b41d\" class=\"link\"> Prekių ženklai ir tiekėjai
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"19\" id=\"subtab-AdminAttachments\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminAttachments&amp;token=87bc44a5ba56dd5ddb6f3a64b2648876\" class=\"link\"> Failai
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"20\" id=\"subtab-AdminParentCartRules\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminCartRules&amp;token=3aca16ebc64e4d0bb6d20f3a2b0d7519\" class=\"link\"> Nuolaidos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"23\" id=\"subtab-AdminStockManagement\">
                              <a href=\"/admin786i0moky/index.php/stock/?_token=NeiSOjlZRR3qgjA2POIi9wIK6IBwZ64oANabLb6N1ig\" class=\"link\"> Stocks
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"24\" id=\"subtab-AdminParentCustomer\">
                  <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminCustomers&amp;token=eb3a0027b3099246fd7e4202bbe51ffa\" class=\"link\">
                    <i class=\"material-icons\">account_circle</i>
                    <span>
                    Klientai
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-24\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"25\" id=\"subtab-AdminCustomers\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminCustomers&amp;token=eb3a0027b3099246fd7e4202bbe51ffa\" class=\"link\"> Klientai
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"26\" id=\"subtab-AdminAddresses\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminAddresses&amp;token=63879c81cac92a6f82a256eab25477c0\" class=\"link\"> Adresai
                              </a>
                            </li>

                                                                                                                          </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"28\" id=\"subtab-AdminParentCustomerThreads\">
                  <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminCustomerThreads&amp;token=fbddd3dd1b04b3ff67c5b5e96e3102af\" class=\"link\">
                    <i class=\"material-icons\">chat</i>
                    <span>
                    Klientų aptarnavimas
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-28\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"29\" id=\"subtab-AdminCustomerThreads\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminCustomerThreads&amp;token=fbddd3dd1b04b3ff67c5b5e96e3102af\" class=\"link\"> Klientų aptarnavimas
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"30\" id=\"subtab-AdminOrderMessage\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminOrderMessage&amp;token=3ded71e50f1335407adfb04a77d959f2\" class=\"link\"> Užsakymo pranešimai
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"31\" id=\"subtab-AdminReturn\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminReturn&amp;token=98cee5e499fae974c2ed0c5bb4a8c1f7\" class=\"link\"> Prekių grąžinimai
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"32\" id=\"subtab-AdminStats\">
                  <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminStats&amp;token=fc402d21533716abd2253b487f4b8347\" class=\"link\">
                    <i class=\"material-icons\">assessment</i>
                    <span>
                    Statistika
                                        </span>

                  </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title d-none d-sm-block -active\" data-submenu=\"42\" id=\"tab-IMPROVE\">
              <span class=\"title\">Pritaikymai</span>
          </li>

                          
                
                                
                <li class=\"link-levelone -active\" data-submenu=\"43\" id=\"subtab-AdminParentModulesSf\">
                  <a href=\"/admin786i0moky/index.php/module/catalog?_token=NeiSOjlZRR3qgjA2POIi9wIK6IBwZ64oANabLb6N1ig\" class=\"link\">
                    <i class=\"material-icons\">extension</i>
                    <span>
                    Moduliai
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-43\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"44\" id=\"subtab-AdminModulesSf\">
                              <a href=\"/admin786i0moky/index.php/module/catalog?_token=NeiSOjlZRR3qgjA2POIi9wIK6IBwZ64oANabLb6N1ig\" class=\"link\"> Moduliai ir paslaugos
                              </a>
                            </li>

                                                                                                                              
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"46\" id=\"subtab-AdminAddonsCatalog\">
                              <a href=\"/admin786i0moky/index.php/module/addons-store?_token=NeiSOjlZRR3qgjA2POIi9wIK6IBwZ64oANabLb6N1ig\" class=\"link\"> Modulių katalogas
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"47\" id=\"subtab-AdminParentThemes\">
                  <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminThemes&amp;token=8c9a2582f968dc65537c00a5ee3f7664\" class=\"link\">
                    <i class=\"material-icons\">desktop_mac</i>
                    <span>
                    Išvaizda
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-47\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"48\" id=\"subtab-AdminThemes\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminThemes&amp;token=8c9a2582f968dc65537c00a5ee3f7664\" class=\"link\"> Tema ir logotipas
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"49\" id=\"subtab-AdminThemesCatalog\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminThemesCatalog&amp;token=e503fe494db8f9a53a4241c6e93c88e1\" class=\"link\"> Temos katalogas
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"50\" id=\"subtab-AdminCmsContent\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminCmsContent&amp;token=7798091bcbb414937ddcd1ffafbfb200\" class=\"link\"> Puslapiai
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"51\" id=\"subtab-AdminModulesPositions\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminModulesPositions&amp;token=165060204a696e48ade2c66fc3c06dbc\" class=\"link\"> Pozicijos
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"52\" id=\"subtab-AdminImages\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminImages&amp;token=6c710c2fa9ddec049ab030eb2cfab26b\" class=\"link\"> Paveiksliukų nustatymai
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"117\" id=\"subtab-AdminLinkWidget\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminLinkWidget&amp;token=26fa6979fac1697e4d2645d7d5f00dbe\" class=\"link\"> Link Widget
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"53\" id=\"subtab-AdminParentShipping\">
                  <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminCarriers&amp;token=b608522754cd09aa23bd49924189d4ae\" class=\"link\">
                    <i class=\"material-icons\">local_shipping</i>
                    <span>
                    Pristatymas
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-53\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"54\" id=\"subtab-AdminCarriers\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminCarriers&amp;token=b608522754cd09aa23bd49924189d4ae\" class=\"link\"> Kurjeriai
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"55\" id=\"subtab-AdminShipping\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminShipping&amp;token=c881d182d6737ad558859be1a5b8e8ba\" class=\"link\"> Nustatymai
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"56\" id=\"subtab-AdminParentPayment\">
                  <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminPayment&amp;token=c87b790a247016acf4a38de2520fded8\" class=\"link\">
                    <i class=\"material-icons\">payment</i>
                    <span>
                    Mokėjimas
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-56\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"57\" id=\"subtab-AdminPayment\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminPayment&amp;token=c87b790a247016acf4a38de2520fded8\" class=\"link\"> Mokėjimo būdai
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"58\" id=\"subtab-AdminPaymentPreferences\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminPaymentPreferences&amp;token=04973b5e488a9bb6c83ce194d0505bde\" class=\"link\"> Nustatymai
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"59\" id=\"subtab-AdminInternational\">
                  <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminLocalization&amp;token=84919f31eab5619594b7ef54f0a831e1\" class=\"link\">
                    <i class=\"material-icons\">language</i>
                    <span>
                    Tarptautinis
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-59\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"60\" id=\"subtab-AdminParentLocalization\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminLocalization&amp;token=84919f31eab5619594b7ef54f0a831e1\" class=\"link\"> Lokalizacija
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"65\" id=\"subtab-AdminParentCountries\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminZones&amp;token=db10ecf99565b976e482133a9245d095\" class=\"link\"> Vietovės
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"69\" id=\"subtab-AdminParentTaxes\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminTaxes&amp;token=574114d00392008e978a97bb0ef40d11\" class=\"link\"> Mokesčiai
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"72\" id=\"subtab-AdminTranslations\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminTranslations&amp;token=2e4dabf858ddbb7bfd87354fb927c465\" class=\"link\"> Vertimai
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"127\" id=\"subtab-Adminxprtdashboard\">
                  <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=Adminxippost&amp;token=3c242b4ccaabc55a9046683c5a559984\" class=\"link\">
                    <i class=\"material-icons\"></i>
                    <span>
                    Xpert Blog
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-127\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"128\" id=\"subtab-Adminxippost\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=Adminxippost&amp;token=3c242b4ccaabc55a9046683c5a559984\" class=\"link\"> Blog Posts
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"129\" id=\"subtab-Adminxipcategory\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=Adminxipcategory&amp;token=aa00675cb788494a2182b63b441667eb\" class=\"link\"> Blog Categories
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"130\" id=\"subtab-Adminxipcomment\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=Adminxipcomment&amp;token=20fb68950bffaeae490a810e13d6147f\" class=\"link\"> Blog Comments
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"131\" id=\"subtab-Adminxipimagetype\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=Adminxipimagetype&amp;token=99ce6cc014f12615a85089b4a337ac46\" class=\"link\"> Blog Image Type
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title d-none d-sm-block \" data-submenu=\"73\" id=\"tab-CONFIGURE\">
              <span class=\"title\">Konfigūruoti</span>
          </li>

                          
                
                                
                <li class=\"link-levelone \" data-submenu=\"74\" id=\"subtab-ShopParameters\">
                  <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminPreferences&amp;token=cd166ce3dfbfc649974b6abd87dab59c\" class=\"link\">
                    <i class=\"material-icons\">settings</i>
                    <span>
                    Parduotuvės nustatymai
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-74\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"75\" id=\"subtab-AdminParentPreferences\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminPreferences&amp;token=cd166ce3dfbfc649974b6abd87dab59c\" class=\"link\"> Pagrindiniai
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"78\" id=\"subtab-AdminParentOrderPreferences\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminOrderPreferences&amp;token=3ab6005833c7345607e4a109a7f9055d\" class=\"link\"> Užsakymų nustatymai
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"81\" id=\"subtab-AdminPPreferences\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminPPreferences&amp;token=20fce78a6d1497627775e041a10163cf\" class=\"link\"> Prekės
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"82\" id=\"subtab-AdminParentCustomerPreferences\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminCustomerPreferences&amp;token=25b34d9826fefab6ba636f2e9b6f8ac8\" class=\"link\"> Klientų nustatymai
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"86\" id=\"subtab-AdminParentStores\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminContacts&amp;token=f789acd3c15c2a848db063c86047aaa8\" class=\"link\"> Kontaktai
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"89\" id=\"subtab-AdminParentMeta\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminMeta&amp;token=817617daedbf614335db9ef6b53fc9f3\" class=\"link\"> Duomenų srautas ir SEO
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"93\" id=\"subtab-AdminParentSearchConf\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminSearchConf&amp;token=a216b8195d5bec2eba7c1f9b7997c475\" class=\"link\"> Paieška
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"119\" id=\"subtab-AdminGamification\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminGamification&amp;token=c27dbf211d520b9a19cf9075b2b9eb42\" class=\"link\"> Merchant Expertise
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"96\" id=\"subtab-AdminAdvancedParameters\">
                  <a href=\"/admin786i0moky/index.php/configure/advanced/system_information?_token=NeiSOjlZRR3qgjA2POIi9wIK6IBwZ64oANabLb6N1ig\" class=\"link\">
                    <i class=\"material-icons\">settings_applications</i>
                    <span>
                    Išplėstiniai parametrai
                                          <i class=\"material-icons float-right d-md-none\">keyboard_arrow_down</i>
                                        </span>

                  </a>
                                          <ul id=\"collapse-96\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"97\" id=\"subtab-AdminInformation\">
                              <a href=\"/admin786i0moky/index.php/configure/advanced/system_information?_token=NeiSOjlZRR3qgjA2POIi9wIK6IBwZ64oANabLb6N1ig\" class=\"link\"> Informacija
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"98\" id=\"subtab-AdminPerformance\">
                              <a href=\"/admin786i0moky/index.php/configure/advanced/performance?_token=NeiSOjlZRR3qgjA2POIi9wIK6IBwZ64oANabLb6N1ig\" class=\"link\"> Našumas
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"99\" id=\"subtab-AdminAdminPreferences\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminAdminPreferences&amp;token=8fe62720e975c3e97c63ea0655bb66e1\" class=\"link\"> Administracija
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"100\" id=\"subtab-AdminEmails\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminEmails&amp;token=ea77ae054332db21212e3a4703a381c0\" class=\"link\"> El. paštas
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"101\" id=\"subtab-AdminImport\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminImport&amp;token=e78a4989f3877c673dd02c97e2d0f070\" class=\"link\"> Importuoti
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"102\" id=\"subtab-AdminParentEmployees\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminEmployees&amp;token=1a8d9ab18d7db7f78b62289b4862e360\" class=\"link\"> Darbuotojai
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"106\" id=\"subtab-AdminParentRequestSql\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminRequestSql&amp;token=e13574123928f1ff3db31757af711f6a\" class=\"link\"> Duomenų bazė
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"109\" id=\"subtab-AdminLogs\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminLogs&amp;token=8986e4ff3c44db17f01d196c1b2b49a1\" class=\"link\"> Įvykių žurnalas
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"110\" id=\"subtab-AdminWebservice\">
                              <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminWebservice&amp;token=17e7fb2cf4cda972d31bf7e4e22c9229\" class=\"link\"> Webservice&#039;as
                              </a>
                            </li>

                                                                                                                                                                            </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title d-none d-sm-block \" data-submenu=\"121\" id=\"tab-AdminPosMenu\">
              <span class=\"title\">PosExtentions</span>
          </li>

                          
                
                                
                <li class=\"link-levelone \" data-submenu=\"122\" id=\"subtab-AdminTestimonials\">
                  <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminTestimonials&amp;token=6a16004c27fcc104c0e2d2f73dcd8922\" class=\"link\">
                    <i class=\"material-icons\"></i>
                    <span>
                    Manage Testimonials
                                        </span>

                  </a>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"123\" id=\"subtab-AdminPosLogo\">
                  <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminPosLogo&amp;token=e862207f34cbcc7faf9bbf60d570ac1f\" class=\"link\">
                    <i class=\"material-icons\"></i>
                    <span>
                    Manage Logo
                                        </span>

                  </a>
                                    </li>
                          
        
            </ul>

  <span class=\"menu-collapse d-none d-md-block\">
    <i class=\"material-icons\">&#xE8EE;</i>
  </span>

  
</nav>


<div id=\"main-div\">

  
    
<div class=\"header-toolbar\">

  
    <nav class=\"breadcrumb\">

                        <a class=\"breadcrumb-item\" href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminParentModulesSf&amp;token=b13c0669211bd3dff74c32138b0fc757\">Moduliai</a>
              
      
    </nav>
  

  
    <h2 class=\"title\">
      Modulio pasirinkimas    </h2>
  

  
    <div class=\"toolbar-icons\">
      
                        
          <a
            class=\"mx-1 btn btn-primary  pointer\"            id=\"page-header-desc-configuration-add_module\"
            href=\"#\"            title=\"Įkelti modulį\"            data-toggle=\"pstooltip\"
            data-placement=\"bottom\"          >
            <i class=\"material-icons\">cloud_upload</i>
            <span class=\"title\">Įkelti modulį</span>
          </a>
                                
          <a
            class=\"mx-1 btn btn-primary  pointer\"            id=\"page-header-desc-configuration-addons_connect\"
            href=\"#\"            title=\"Prisijungti prie Addons prekyvietės\"            data-toggle=\"pstooltip\"
            data-placement=\"bottom\"          >
            <i class=\"material-icons\">vpn_key</i>
            <span class=\"title\">Prisijungti prie Addons prekyvietės</span>
          </a>
                          
                  <a class=\"toolbar-button btn-help btn-sidebar\" href=\"#\"
             title=\"Pagalba\"
             data-toggle=\"sidebar\"
             data-target=\"#right-sidebar\"
             data-url=\"/admin786i0moky/index.php/common/sidebar/http%253A%252F%252Fhelp.prestashop.com%252Flt%252Fdoc%252FAdminModules%253Fversion%253D1.7.3.1%2526country%253Dlt/Pagalba?_token=NeiSOjlZRR3qgjA2POIi9wIK6IBwZ64oANabLb6N1ig\"
             id=\"product_form_open_help\"
          >
            <i class=\"material-icons\">help</i>
            <span class=\"title\">Pagalba</span>
          </a>
                  </div>
  
        <div class=\"page-head-tabs\" id=\"head_tabs\">
                <a class=\"tab current\"
   href=\"/admin786i0moky/index.php/module/catalog?_token=NeiSOjlZRR3qgjA2POIi9wIK6IBwZ64oANabLb6N1ig\">Pasirinkimas</a>

                <a class=\"tab\"
   href=\"/admin786i0moky/index.php/module/manage?_token=NeiSOjlZRR3qgjA2POIi9wIK6IBwZ64oANabLb6N1ig\">Įdiegti moduliai</a>

                <a class=\"tab\"
   href=\"/admin786i0moky/index.php/module/notifications?_token=NeiSOjlZRR3qgjA2POIi9wIK6IBwZ64oANabLb6N1ig\">Pranešimai  <div class=\"notification-container\">
    <span class=\"notification-counter\">4</span>
  </div>
  </a>

            </div>
      
</div>
    <div class=\"modal fade\" id=\"modal_addons_connect\" tabindex=\"-1\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
\t\t\t\t<h4 class=\"modal-title\"><i class=\"icon-puzzle-piece\"></i> <a target=\"_blank\" href=\"http://addons.prestashop.com/?utm_source=back-office&utm_medium=modules&utm_campaign=back-office-LT&utm_content=download\">PrestaShop Addons</a></h4>
\t\t\t</div>
\t\t\t
\t\t\t
<div class=\"modal-body\">
\t\t\t\t\t\t<!--start addons login-->
\t\t\t<form id=\"addons_login_form\" method=\"post\" >
\t\t\t\t<div>
\t\t\t\t\t<a href=\"https://addons.prestashop.com/lt/login?email=airidas.beno%40gmail.com&amp;firstname=Airidas&amp;lastname=Benokraitis&amp;website=http%3A%2F%2Fautokrai.kraitis.lt%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-LT&amp;utm_content=download#createnow\"><img class=\"img-responsive center-block\" src=\"/admin786i0moky/themes/default/img/prestashop-addons-logo.png\" alt=\"Logo PrestaShop Addons\"/></a>
\t\t\t\t\t<h3 class=\"text-center\">Prijunkite savo parduotuvę prie PrestaShop prekyvietės tam, kad galėtumėte automatiškai importuoti visus pirkimus iš Addons.</h3>
\t\t\t\t\t<hr />
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Neturite paskyros?</h4>
\t\t\t\t\t\t<p class='text-justify'>Atraskite PrestaShop Addons jėgą! Naršykite PrestaShop oficialioje prekyvietėje ir rinkitės tarp 3500 skirtingų modulių. Išsirinkite parduotuvės temą, pagerinkite konversijos santykį, padidinkite srautus, suteikite vartotojams lojalumo apdovanojimus ir pagerinkite produktyvumą</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Prisijungti prie PrestaShop Addons</h4>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><i class=\"icon-user\"></i></span>
\t\t\t\t\t\t\t\t<input id=\"username_addons\" name=\"username_addons\" type=\"text\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><i class=\"icon-key\"></i></span>
\t\t\t\t\t\t\t\t<input id=\"password_addons\" name=\"password_addons\" type=\"password\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<a class=\"btn btn-link float-right _blank\" href=\"//addons.prestashop.com/lt/forgot-your-password\">Aš pamiršau savo slaptažodį</a>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"row row-padding-top\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<a class=\"btn btn-default btn-block btn-lg _blank\" href=\"https://addons.prestashop.com/lt/login?email=airidas.beno%40gmail.com&amp;firstname=Airidas&amp;lastname=Benokraitis&amp;website=http%3A%2F%2Fautokrai.kraitis.lt%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-LT&amp;utm_content=download#createnow\">
\t\t\t\t\t\t\t\tSukurti paskyrą
\t\t\t\t\t\t\t\t<i class=\"icon-external-link\"></i>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<button id=\"addons_login_button\" class=\"btn btn-primary btn-block btn-lg\" type=\"submit\">
\t\t\t\t\t\t\t\t<i class=\"icon-unlock\"></i> Prisijungti
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div id=\"addons_loading\" class=\"help-block\"></div>

\t\t\t</form>
\t\t\t<!--end addons login-->
\t\t\t</div>


\t\t\t\t\t</div>
\t</div>
</div>

    <div class=\"content-div \">

      

      

      

      
      
      
      

      <div class=\"row \">
        <div class=\"col-sm-12\">
          <div id=\"ajax_confirmation\" class=\"alert alert-success\" style=\"display: none;\"></div>




  ";
        // line 1199
        $this->displayBlock('content_header', $context, $blocks);
        // line 1200
        echo "                 ";
        $this->displayBlock('content', $context, $blocks);
        // line 1201
        echo "                 ";
        $this->displayBlock('content_footer', $context, $blocks);
        // line 1202
        echo "                 ";
        $this->displayBlock('sidebar_right', $context, $blocks);
        // line 1203
        echo "
        </div>
      </div>

    </div>

  
</div>

<div id=\"non-responsive\" class=\"js-non-responsive\">
  <h1>O ne!</h1>
  <p class=\"mt-3\">
    Šio puslapio mobili versija šiuo metu negalima.
  </p>
  <p class=\"mt-2\">
    Norėdami matyti šį puslapį naudokite kompiuterį tol, kol jis bus pritaikytas mobiliems įrenginiams.
  </p>
  <p class=\"mt-2\">
    Ačiū.
  </p>
  <a href=\"http://autokrai.kraitis.lt/admin786i0moky/index.php?controller=AdminDashboard&amp;token=22f4e1634756351f72549d53595eb1f4\" class=\"btn btn-primary py-1 mt-3\">
    <i class=\"material-icons\">arrow_back</i>
    Atgal
  </a>
</div>
<div class=\"mobile-layer\"></div>



  <div id=\"footer\" class=\"bootstrap\">
    
</div>



  <div class=\"bootstrap\">
    <div class=\"modal fade\" id=\"modal_addons_connect\" tabindex=\"-1\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
\t\t\t\t<h4 class=\"modal-title\"><i class=\"icon-puzzle-piece\"></i> <a target=\"_blank\" href=\"http://addons.prestashop.com/?utm_source=back-office&utm_medium=modules&utm_campaign=back-office-LT&utm_content=download\">PrestaShop Addons</a></h4>
\t\t\t</div>
\t\t\t
\t\t\t
<div class=\"modal-body\">
\t\t\t\t\t\t<!--start addons login-->
\t\t\t<form id=\"addons_login_form\" method=\"post\" >
\t\t\t\t<div>
\t\t\t\t\t<a href=\"https://addons.prestashop.com/lt/login?email=airidas.beno%40gmail.com&amp;firstname=Airidas&amp;lastname=Benokraitis&amp;website=http%3A%2F%2Fautokrai.kraitis.lt%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-LT&amp;utm_content=download#createnow\"><img class=\"img-responsive center-block\" src=\"/admin786i0moky/themes/default/img/prestashop-addons-logo.png\" alt=\"Logo PrestaShop Addons\"/></a>
\t\t\t\t\t<h3 class=\"text-center\">Prijunkite savo parduotuvę prie PrestaShop prekyvietės tam, kad galėtumėte automatiškai importuoti visus pirkimus iš Addons.</h3>
\t\t\t\t\t<hr />
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Neturite paskyros?</h4>
\t\t\t\t\t\t<p class='text-justify'>Atraskite PrestaShop Addons jėgą! Naršykite PrestaShop oficialioje prekyvietėje ir rinkitės tarp 3500 skirtingų modulių. Išsirinkite parduotuvės temą, pagerinkite konversijos santykį, padidinkite srautus, suteikite vartotojams lojalumo apdovanojimus ir pagerinkite produktyvumą</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Prisijungti prie PrestaShop Addons</h4>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><i class=\"icon-user\"></i></span>
\t\t\t\t\t\t\t\t<input id=\"username_addons\" name=\"username_addons\" type=\"text\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><i class=\"icon-key\"></i></span>
\t\t\t\t\t\t\t\t<input id=\"password_addons\" name=\"password_addons\" type=\"password\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<a class=\"btn btn-link float-right _blank\" href=\"//addons.prestashop.com/lt/forgot-your-password\">Aš pamiršau savo slaptažodį</a>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"row row-padding-top\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<a class=\"btn btn-default btn-block btn-lg _blank\" href=\"https://addons.prestashop.com/lt/login?email=airidas.beno%40gmail.com&amp;firstname=Airidas&amp;lastname=Benokraitis&amp;website=http%3A%2F%2Fautokrai.kraitis.lt%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-LT&amp;utm_content=download#createnow\">
\t\t\t\t\t\t\t\tSukurti paskyrą
\t\t\t\t\t\t\t\t<i class=\"icon-external-link\"></i>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<button id=\"addons_login_button\" class=\"btn btn-primary btn-block btn-lg\" type=\"submit\">
\t\t\t\t\t\t\t\t<i class=\"icon-unlock\"></i> Prisijungti
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div id=\"addons_loading\" class=\"help-block\"></div>

\t\t\t</form>
\t\t\t<!--end addons login-->
\t\t\t</div>


\t\t\t\t\t</div>
\t</div>
</div>

  </div>

";
        // line 1311
        $this->displayBlock('javascripts', $context, $blocks);
        $this->displayBlock('extra_javascripts', $context, $blocks);
        $this->displayBlock('translate_javascripts', $context, $blocks);
        echo "</body>
</html>";
    }

    // line 83
    public function block_stylesheets($context, array $blocks = array())
    {
    }

    public function block_extra_stylesheets($context, array $blocks = array())
    {
    }

    // line 1199
    public function block_content_header($context, array $blocks = array())
    {
    }

    // line 1200
    public function block_content($context, array $blocks = array())
    {
    }

    // line 1201
    public function block_content_footer($context, array $blocks = array())
    {
    }

    // line 1202
    public function block_sidebar_right($context, array $blocks = array())
    {
    }

    // line 1311
    public function block_javascripts($context, array $blocks = array())
    {
    }

    public function block_extra_javascripts($context, array $blocks = array())
    {
    }

    public function block_translate_javascripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "__string_template__d6d5286ad6249b4aa0ed2d6b53adee7e964a2373d05eb1641c788140406f1bee";
    }

    public function getDebugInfo()
    {
        return array (  1390 => 1311,  1385 => 1202,  1380 => 1201,  1375 => 1200,  1370 => 1199,  1361 => 83,  1353 => 1311,  1243 => 1203,  1240 => 1202,  1237 => 1201,  1234 => 1200,  1232 => 1199,  112 => 83,  28 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "__string_template__d6d5286ad6249b4aa0ed2d6b53adee7e964a2373d05eb1641c788140406f1bee", "");
    }
}
