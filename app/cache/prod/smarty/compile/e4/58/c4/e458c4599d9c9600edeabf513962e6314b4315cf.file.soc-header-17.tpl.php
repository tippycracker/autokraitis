<?php /* Smarty version Smarty-3.1.19, created on 2018-04-11 22:07:54
         compiled from "modules/prestasocialstream/views/templates/front/soc-header-17.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9039631735ace5d0ab8cec8-92781261%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e458c4599d9c9600edeabf513962e6314b4315cf' => 
    array (
      0 => 'modules/prestasocialstream/views/templates/front/soc-header-17.tpl',
      1 => 1523463310,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9039631735ace5d0ab8cec8-92781261',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'psversion' => 0,
    'module_dir' => 0,
    'c4t' => 0,
    'li1' => 0,
    'c4' => 0,
    'c5t' => 0,
    'li2' => 0,
    'c5' => 0,
    'c51' => 0,
    'c10' => 0,
    'li3' => 0,
    'c12' => 0,
    'li4' => 0,
    'c121' => 0,
    'c13' => 0,
    'li5' => 0,
    'c13t' => 0,
    'c122' => 0,
    'c14' => 0,
    'li6' => 0,
    'c123' => 0,
    'widthcs' => 0,
    'c11' => 0,
    'bad' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ace5d0acb2467_04110650',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ace5d0acb2467_04110650')) {function content_5ace5d0acb2467_04110650($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/home/autokr/domains/autokrai.kraitis.lt/public_html/vendor/prestashop/smarty/plugins/modifier.replace.php';
?>
<?php if ($_smarty_tpl->tpl_vars['psversion']->value<"1.4.0.0") {?>
<link href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8');?>
css/main.css" rel="stylesheet" type="text/css">
<?php }?>

<script>
(function(){"use strict";var c=[],f={},a,e,d,b;if(!window.jQuery){a=function(g){c.push(g)};f.ready=function(g){a(g)};e=window.jQuery=window.$=function(g){if(typeof g=="function"){a(g)}return f};window.checkJQ=function(){if(!d()){b=setTimeout(checkJQ,100)}};b=setTimeout(checkJQ,100);d=function(){if(window.jQuery!==e){clearTimeout(b);var g=c.shift();while(g){jQuery(g);g=c.shift()}b=f=a=e=d=window.checkJQ=null;return true}return false}}})();
</script>


<script>
    $(document).ready(function(){
				

        $('.social-feed-container').socialfeed({
                    // FACEBOOK
					 plugin_folder:'<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8');?>
',
					  template:'<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8');?>
template.html',
                  <?php if ($_smarty_tpl->tpl_vars['c4t']->value!='') {?>  facebook:{
                          accounts:[<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['c4t']->value,'&#039;',"'");?>
],
                        limit: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['li1']->value, ENT_QUOTES, 'UTF-8');?>
,
                        access_token: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['c4']->value, ENT_QUOTES, 'UTF-8');?>
' // APP_ID|APP_SECRET
                    },<?php }?>
                    // TWITTER
                   <?php if ($_smarty_tpl->tpl_vars['c5t']->value!='') {?>   twitter:{
               accounts: [<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['c5t']->value,'&#039;',"'");?>
],
                        limit: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['li2']->value, ENT_QUOTES, 'UTF-8');?>
,
                        consumer_key: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['c5']->value, ENT_QUOTES, 'UTF-8');?>
', // make sure to have your app read-only
                        consumer_secret: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['c51']->value, ENT_QUOTES, 'UTF-8');?>
', // make sure to have your app read-only
                     },<?php }?>
					
                    // VK
                <?php if ($_smarty_tpl->tpl_vars['c10']->value!='') {?>      vk:{
                              accounts: [<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['c10']->value,'&#039;',"'");?>
], 
                        limit: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['li3']->value, ENT_QUOTES, 'UTF-8');?>
,
                        source: 'all'
                    },<?php }?>
                    // GOOGLEPLUS
                <?php if ($_smarty_tpl->tpl_vars['c12']->value!='') {?>      google:{
                       accounts: [<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['c12']->value,'&#039;',"'");?>
],
                         limit:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['li4']->value, ENT_QUOTES, 'UTF-8');?>
,
                         access_token: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['c121']->value, ENT_QUOTES, 'UTF-8');?>
'
                     },<?php }?>
                    // INSTAGRAM
                <?php if ($_smarty_tpl->tpl_vars['c13']->value!='') {?>         instagram:{
                       accounts: [<?php echo smarty_modifier_replace(smarty_modifier_replace($_smarty_tpl->tpl_vars['c13']->value,'&#039;',"'"),'&amp;','&');?>
],
                        limit:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['li5']->value, ENT_QUOTES, 'UTF-8');?>
,
                        client_id: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['c13t']->value, ENT_QUOTES, 'UTF-8');?>
',
						   access_token: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['c122']->value, ENT_QUOTES, 'UTF-8');?>
'
                    },<?php }?>
					 //pinterest
					  <?php if ($_smarty_tpl->tpl_vars['c14']->value!='') {?>         pinterest:{
                       accounts: [<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['c14']->value,'&#039;',"'");?>
],
                        limit:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['li6']->value, ENT_QUOTES, 'UTF-8');?>
,
                        //client_id: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['c13t']->value, ENT_QUOTES, 'UTF-8');?>
',
                        access_token: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['c123']->value, ENT_QUOTES, 'UTF-8');?>
'

                    },<?php }?> 
                  //  blogspot:{
                  //      accounts:['@greenworldmultilevel']
                  //  },
                    // GENERAL SETTINGS
                    length:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['widthcs']->value, ENT_QUOTES, 'UTF-8');?>
,
                    show_media:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['c11']->value, ENT_QUOTES, 'UTF-8');?>
,
                    // Moderation function - if returns false, template will have class hidden
                    moderation: function(content){
                        return  (content.text) ? content.text.indexOf('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['bad']->value, ENT_QUOTES, 'UTF-8');?>
') == -1 : true;
                    },
                    //update_period: 5000,
                    // When all the posts are collected and displayed - this function is evoked
                    callback: function(){
                        console.log('all posts are collected');
                    }
                });
				$(".social-feed-text a:contains('read more')").html("<?php echo smartyTranslate(array('s'=>'Leer','mod'=>strip_tags('prestasocialstream')),$_smarty_tpl);?>
");

				moment().format('L');
        });
</script>
<?php }} ?>
